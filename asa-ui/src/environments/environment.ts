// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////// LOCAL /////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////
export const environment = {
  security_api: {
    app_url: 'https://security-admin-api-dev.bbaaviation.net',
    client_id: 'svc-sec-web-dev',
    client_secret: 'vVmSejrLA!',
    //client_id: "svc-sec-web-test",
    //client_secret: "GjkIUvWdA!",
    key: 'sec',
    userKey: 'currentUser-sec',
    userId: 'id'
  },

  production: false,
  app_url: 'https://security-admin-api-dev.bbaaviation.net',
  client_id: 'svc-sec-web-dev',
  client_secret: 'vVmSejrLA!'
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
