import { Component, Input, OnInit } from '@angular/core';
import { PrincipalProfile } from '../../service/admin.service';
import { AuthService } from '../../service/auth.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  appName: string = Constants.AppName;
  appPath: string = Constants.AppPathSeparator;
  profile: PrincipalProfile | undefined;
  userName: string | undefined = '';
  @Input()
  set setbreadCrumb(value: string) {
    this.appPath = Constants.AppPathSeparator + value;
  }
  constructor(private authService: AuthService) {
    console.log(Constants.AppName);
  }

  ngOnInit(): void {
    setTimeout(() => { this.getProfile(); }, 100);
  }

  getProfile() {
    this.profile = new PrincipalProfile();
    this.authService.getProfile()?.subscribe(profile => {
      console.log('consuming profile:' + profile.name);
      this.profile = profile;
      this.userName = profile.name;
    });
  }

  logout() {
    this.authService.logout(true);
  }

  getProfileInitials() {
    if (this.profile === null || this.profile === undefined ||
      this.profile.name === null || this.profile.name === undefined) {
      return "";
    }

    var array = this.profile.name.split(/\s+/);

    if (array.length > 1 && array[0].length > 1 && array[1].length > 1) {
      return array[0].charAt(0).toUpperCase() + array[1].charAt(0).toUpperCase();
    }

    return "";
  }
}
