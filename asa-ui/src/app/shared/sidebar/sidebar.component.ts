import { animate, AUTO_STYLE, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ElementRef, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { MasterComponent } from '../../admin/master/master.component';
import { Realm, RealmSearchRequest, ASAPermissionService } from '../../service/admin.service';
import { CacheService } from '../../service/cache.service';
import { CommonService } from '../../service/common.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  animations: [
    trigger('collapse', [
      state('false', style({ height: AUTO_STYLE, visibility: AUTO_STYLE })),
      state('true', style({ height: '0', visibility: 'hidden' })),
      transition('false => true', animate(300 + 'ms ease-in')),
      transition('true => false', animate(300 + 'ms ease-out'))
    ]),
    trigger(
      'inOutAnimation',
      [
        transition(
          ':enter',
          [
            style({ opacity: 0 }),
            animate('300ms ease-out',
              style({ opacity: 1 }))
          ]
        ),
        transition(
          ':leave',
          [
            style({ opacity: 0 }),
            animate('100ms ease-in',
              style({ opacity: 0 }))
          ]
        )
      ]
    )
  ]
})
export class SidebarComponent extends MasterComponent implements OnInit  {
  realms: Realm[] | undefined;
  @Output() handleSideBar = new EventEmitter<string>();
  minibar: boolean = false;
  loggedIn: boolean = false;
  collapseManage: boolean = true;
  @Input()
  set setSideBar(value: boolean) {
    this.minibar = <boolean><unknown>value;
  }

  realmEditId: string = '';
  selectedRealmId: string = '';
  constructor(private cacheService: CacheService,
    public commonService: CommonService,
    private asaPermissionService: ASAPermissionService ) {
    super(commonService);
    //setTimeout(() => { this.toggleSidebar(); }, 0);
    //this.showSideBar();
    this.checkIfLoggedIn();
    this.loadRealms();
  }

  checkIfLoggedIn() {
    let userSettings = this.cacheService.getUserSettings();
    if (userSettings == null || userSettings == undefined)
      setTimeout(() => { this.checkIfLoggedIn(); }, 100);
    else {
      this.loggedIn = true;
    }
  }

  showSideBar() {
    console.log('showSideBar inited')

    setTimeout(() => {
      console.log('timer started')
      let userSettings = this.cacheService.getUserSettings();
      if (userSettings == null)
        this.showSideBar();
      else {
        this.minibar = userSettings?.showSideBar == 'false';
        console.log('this.minibar', this.minibar);
        this.loggedIn = true;
      }
      }, 100);
  }
  ngOnInit(): void {
    //this.showSideBar();
    let ls_collapseManage = localStorage.getItem(Constants.LocalStorage_ManageMenuKey)
    if (ls_collapseManage != undefined && ls_collapseManage != '')
      this.collapseManage = (/true/i).test((ls_collapseManage ?? 'false').toString());
    console.log('this.collapseManage', this.collapseManage);
    console.log('this.collapseManage1', ls_collapseManage);
  }

  toggleSidebar() {
    console.log('handled');
    this.minibar = !this.minibar;

    this.handleSideBar.next(this.minibar.toString());
  }

  highlightMenu(obj: any) {
    console.log(obj);
  }

  expand(key: string) {
    if (this.minibar == false) {
      this.collapseManage = !this.collapseManage;
      localStorage.setItem(Constants.LocalStorage_ManageMenuKey, this.collapseManage.toString());
    }
  }

  loadRealms() {
    const self = this;
    const request = new RealmSearchRequest();
    this.asaPermissionService.search_For_Realms(request)
      .subscribe({
        next(result) {
          self.realms = result.items;
        }
      });
  }

  selectRealmId(realm: Realm) {
    if (this.selectedRealmId != <string>realm.id)
      this.commonService.setEditRealmFlagSubject('');

    this.selectedRealmId = <string>realm.id;
    if (this.realmEditId != <string>realm.id) this.realmEditId = '';
  }
  toggleEditRealm(realm: Realm) {
    if (this.realmEditId == <string>realm.id) this.realmEditId = '';
    else {
      this.realmEditId = <string>realm.id;
      this.selectRealmId(realm);
    }
    this.commonService.setEditRealmFlagSubject(this.realmEditId);
  }
  getLeftNavClass(realm: Realm) {
    let _class = '';
    if (this.realmEditId == '' && this.selectedRealmId != '' && this.selectedRealmId == realm.id)
      _class = 'c-bg-c-4 c-font-c-7';
    else if (this.realmEditId == '' && this.selectedRealmId != '' && this.selectedRealmId != realm.id)
      _class = 'c-bg-c-5 c-font-c-7';
    else if (this.realmEditId != '' && this.selectedRealmId != '' && this.realmEditId == realm.id)
      _class = 'c-bg-c-3 c-font-c-0';
    return _class;
  }

  addRealm() {
    let modalData = {
      componentKey: Constants.ComponentKeyAddRealm,
      actionKey: Constants.ActionKeyNew
    };
    this.commonService.setGridRowSubject(modalData);
  }
}
