import {Component} from "@angular/core";
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
    selector: 'child-cell',
    template: '<a class="btn btn-success btn-sm"  style="margin:0px" [routerLink]="[\'/realm\', this.params.node.data.realmId, \'role\', this.params.node.data.id, \'edit\']"><i class="nc-icon nc-ruler-pencil"></i></a>' +
    '<a class="btn btn-default btn-sm"  style="margin:0px" [routerLink]="[\'/realm\', this.params.node.data.realmId, \'role\', this.params.node.data.id, \'view\']"><i class="nc-icon nc-minimal-right"></i></a>',
})
export class RoleMenuRenderer implements ICellRendererAngularComp {
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    public invokeParentMethod() {
        this.params.context.componentParent.methodFromParent(`Row: ${this.params.node.rowIndex}, Col: ${this.params.colDef.headerName}`)
    }

    refresh(): boolean {
        return false;
    }
}