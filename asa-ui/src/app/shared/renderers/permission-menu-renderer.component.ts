import {Component} from "@angular/core";
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
    selector: 'child-cell',
    template: '<a class="btn btn-success btn-sm" style="margin:0px" [routerLink]="[\'/realm\', this.params.node.data.realmId, \'permission\', this.params.node.data.id, \'edit\']"><i class="nc-icon nc-ruler-pencil"></i></a>',
})
export class PermissionMenuRenderer implements ICellRendererAngularComp {
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    refresh(): boolean {
        return false;
    }
}