import {Component} from "@angular/core";
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
    selector: 'child-cell',
    template: '<span><a class="btn btn-success btn-sm"  style="margin:0px" href="user\\{{this.params.node.data.id}}\\edit\"><i class="nc-icon nc-ruler-pencil"></i></a>' +
    '<a class="btn btn-default btn-sm"  style="margin:0px" href="user\\{{this.params.node.data.id}}\\edit\"><i class="nc-icon nc-minimal-right"></i></a></span>',
})
export class UserMenuRenderer implements ICellRendererAngularComp {
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    refresh(): boolean {
        return false;
    }
}
