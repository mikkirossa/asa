import { Pipe, PipeTransform } from '@angular/core';
import { CacheService } from '../service/cache.service';

@Pipe({
  name: 'fullUserName'
})
export class FullUserNamePipe implements PipeTransform {
  constructor(private cacheService: CacheService) {
  }

  transform(value: unknown, ...args: unknown[]): unknown {
    let filteredUsers = this.cacheService.getAllUsers()?.filter((p : any) => p.u == value);
    if (filteredUsers != null && filteredUsers.length > 0)
      return filteredUsers[0].l + ', ' + filteredUsers[0].f;
    else
      return value;
  }

}
