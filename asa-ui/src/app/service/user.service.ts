import { Injectable } from '@angular/core';
import { Constants } from '../utils/constants';
import { ASAPermissionService, UserPrincipalSearchRequest, UserSettings } from './admin.service';
import { environment } from '@environment';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private asaPermissionService: ASAPermissionService,
    private authService: AuthService  ) {
  }

  searchUsers(): any {
    let params: any = {};
    params.isActiveOnly = true;
    return null;//this.bbaSecurityAdminClient.search_For_Users(params);
  }

  getUserSettings(): Array<UserSettings> {
    let userSettings = JSON.parse(<string>localStorage.getItem(Constants.User_Settings));
    let appSpecificSettings = (userSettings)?.filter((t: any) => t.key == Constants.User_SettingsKey)[0]?.settings;
    if (appSpecificSettings == null || appSpecificSettings == undefined) {
      let defaultSetting = this.addDefaultUserSetting(<string>localStorage.getItem(environment.security_api.userId))
      userSettings.push(defaultSetting);
    }
    return userSettings;
  }

  addUserSetting(id: string, userSettings: Array<UserSettings>): Observable<void> {
    return this.asaPermissionService.add_User_Settings(id, userSettings);
  }

  removeUserSetting(id: string, userSettings: Array<UserSettings>): Observable<void> {
    return this.asaPermissionService.remove_User_Settings(id, userSettings);
  }

  addDefaultUserSetting(id: string) {

    var userSettings: Array<UserSettings> = new Array<UserSettings>();
    var defaultSettings: any = {};
    Constants.User_SettingProps.forEach(s => {
      defaultSettings[s.split('-')[0]] = s.split('-')[1];
    });

    let setting = new UserSettings();
    setting.key = Constants.User_SettingsKey;
    setting.settings = defaultSettings;

    userSettings.push(setting);

    this.asaPermissionService.add_User_Settings(id, userSettings)
      .subscribe(result => {
        console.log(result);
        
      });
    return setting;
  }

  getAppSpecificSettings() {
    let _settings = (this.getUserSettings())?.filter((t: any) => t.key == Constants.User_SettingsKey)[0]?.settings;
    return _settings;
  }

  doNotShowAddPopUp() {
    let _settings = this.getAppSpecificSettings();
    return (_settings?.doNotShowAddPopUp ?? 'false') == 'true' ? true : false;
  }

  doNotShowSavePopUp() {
    let _settings = this.getAppSpecificSettings();
    return (_settings?.doNotShowSavePopUp) == 'true' ? true : false;
  }

  minimizeSidebar() {
    let _settings = this.getAppSpecificSettings();
    return (_settings?.minimizeSidebar) == 'true' ? true : false;
  }

  toggleSettingProps(props: Array<string>) {
    var userSettings: Array<UserSettings> = new Array<UserSettings>();

    let setting = new UserSettings();
    setting.key = Constants.User_SettingsKey;
    var selectedSettings: any = {};
    props.forEach(p => {
      selectedSettings[p.split('-')[0]] = p.split('-')[1];
    });

    setting.settings = selectedSettings;
    userSettings.push(setting);

    this.addUserSetting(<string>localStorage.getItem(environment.security_api.userId), userSettings)
      .subscribe(result => {
        this.authService.validateToken(true);
        console.log('Settings updated...');
      });
  }

  getUserPermissions() {
    const value = localStorage.getItem(Constants.Permission_Keys);

    if (value != null) {
      return JSON.parse(value);
    }

    return null as any;
  }
}
