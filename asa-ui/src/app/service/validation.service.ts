import { TitleCasePipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { Information, UniqueValidationCriteria } from '../models/common/validationCriteria';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  performUniqueValidation(data: any, existingData: any, criteria: UniqueValidationCriteria): UniqueValidationCriteria {
    let errorCriteria: string = '';
    switch (criteria.matchingFields) {
      case 1: {
        errorCriteria = TitleCasePipe.prototype.transform(criteria.field1) + ' = ' + criteria.field1Value;
        break;
      }
      case 2: { // any one matching
        errorCriteria = TitleCasePipe.prototype.transform(criteria.field1) + ' = ' + criteria.field1Value + ' OR ' + TitleCasePipe.prototype.transform(criteria.field2) + ' = ' + criteria.field2Value;
        break;
      }
      case 22: { // all matching
        errorCriteria = TitleCasePipe.prototype.transform(criteria.field1) + ' = ' + criteria.field1Value + ' AND ' + TitleCasePipe.prototype.transform(criteria.field2) + ' = ' + criteria.field2Value;
        break;
      }
      case 3: { // any one matching
        errorCriteria = TitleCasePipe.prototype.transform(criteria.field1) + ' = ' + criteria.field1Value + ' OR ' + TitleCasePipe.prototype.transform(criteria.field2) + ' = ' + criteria.field2Value + ' OR ' + TitleCasePipe.prototype.transform(criteria.field3) + ' = ' + criteria.field3Value;
        break;
      }
      case 33: {  // all matching
        errorCriteria = TitleCasePipe.prototype.transform(criteria.field1) + ' = ' + criteria.field1Value + ' AND ' + TitleCasePipe.prototype.transform(criteria.field2) + ' = ' + criteria.field2Value + ' AND ' + TitleCasePipe.prototype.transform(criteria.field3) + ' = ' + criteria.field3Value;
        break;
      }
      case 4: { // any one matching
        errorCriteria = TitleCasePipe.prototype.transform(criteria.field1) + ' = ' + criteria.field1Value + ' OR ' + TitleCasePipe.prototype.transform(criteria.field2) + ' = ' + criteria.field2Value + ' OR ' + TitleCasePipe.prototype.transform(criteria.field3) + ' = ' + criteria.field3Value + ' OR ' + TitleCasePipe.prototype.transform(criteria.field4) + ' = ' + criteria.field4Value;
        break;
      }
      case 44: { // all matching
        errorCriteria = TitleCasePipe.prototype.transform(criteria.field1) + ' = ' + criteria.field1Value + ' AND ' + TitleCasePipe.prototype.transform(criteria.field2) + ' = ' + criteria.field2Value + ' AND ' + TitleCasePipe.prototype.transform(criteria.field3) + ' = ' + criteria.field3Value + ' AND ' + TitleCasePipe.prototype.transform(criteria.field4) + ' = ' + (criteria.field4Value ?? null);
        break;
      }
    }

    let filteredData = null;
    if ((<string>criteria.pkField).indexOf('-') > -1) {
      let _pkfs = (<string>criteria.pkField).split('-');
      let _pkvs = (<string>criteria.pkValue).split('-');
      let _pklen = _pkfs.length;
      existingData = existingData.filter((i: any) => {
        if (_pklen == 2)
          return !(i[<string>_pkfs[0]] == Number(_pkvs[0]) && i[<string>_pkfs[1]] == Number(_pkvs[1]));
        else if (_pklen == 3)
          return !(i[<string>_pkfs[0]] == Number(_pkvs[0]) && i[<string>_pkfs[1]] == Number(_pkvs[1]) && i[<string>_pkfs[2]] == Number(_pkvs[2]));
        else if (_pklen == 4)
          return !(i[<string>_pkfs[0]] == Number(_pkvs[0]) && i[<string>_pkfs[1]] == Number(_pkvs[1]) && i[<string>_pkfs[2]] == Number(_pkvs[2]) && i[<string>_pkfs[3]] == Number(_pkvs[3]));
        else
          return !(i[<string>_pkfs[0]] == Number(_pkvs[0]) && i[<string>_pkfs[1]] == Number(_pkvs[1]) && i[<string>_pkfs[2]] == Number(_pkvs[2]) && i[<string>_pkfs[3]] == Number(_pkvs[3]) && i[<string>_pkfs[4]] == Number(_pkvs[4]));
      });
    }
    else if (criteria.pkValue != null && criteria.pkValue != '')
      existingData = existingData.filter((i: any) => i[<string>criteria.pkField] != criteria.pkValue);

    switch (criteria.matchingFields) {
      case 1: {
        filteredData = existingData.filter((p: any) => 
          (p[<string>criteria.field1]?.toString().toUpperCase() ?? null) == (criteria.field1Value?.toUpperCase() ?? null)
        );
        break;
      }
      case 2: { // any one matching
        filteredData = existingData.filter((p: any) => 
          (p[<string>criteria.field1]?.toString().toUpperCase() ?? null) == (criteria.field1Value?.toUpperCase() ?? null) ||
          (p[<string>criteria.field2]?.toString().toUpperCase() ?? null) == (criteria.field2Value?.toUpperCase() ?? null)
        );
        break;
      }
      case 22: { // all matching
        filteredData = existingData.filter((p: any) =>
          (p[<string>criteria.field1]?.toString().toUpperCase() ?? null) == (criteria.field1Value?.toUpperCase() ?? null) &&
          (p[<string>criteria.field2]?.toString().toUpperCase() ?? null) == (criteria.field2Value?.toUpperCase() ?? null)
        );
        break;
      }
      case 3: { // any one matching
        filteredData = existingData.filter((p: any) => 
          (p[<string>criteria.field1]?.toString().toUpperCase() ?? null) == (criteria.field1Value?.toUpperCase() ?? null) ||
          (p[<string>criteria.field2]?.toString().toUpperCase() ?? null) == (criteria.field2Value?.toUpperCase() ?? null) ||
          (p[<string>criteria.field3]?.toString().toUpperCase() ?? null) == (criteria.field3Value?.toUpperCase() ?? null)
        );
        break;
      }
      case 33: {  // all matching
        filteredData = existingData.filter((p: any) =>
          (p[<string>criteria.field1]?.toString().toUpperCase() ?? null) == (criteria.field1Value?.toUpperCase() ?? null) &&
          (p[<string>criteria.field2]?.toString().toUpperCase() ?? null) == (criteria.field2Value?.toUpperCase() ?? null) &&
          (p[<string>criteria.field3]?.toString().toUpperCase() ?? null) == (criteria.field3Value?.toUpperCase() ?? null)
        );
        break;
      }
      case 4: { // any one matching
        filteredData = existingData.filter((p: any) => 
          (p[<string>criteria.field1]?.toString().toUpperCase() ?? null) == (criteria.field1Value?.toUpperCase() ?? null) ||
          (p[<string>criteria.field2]?.toString().toUpperCase() ?? null) == (criteria.field2Value?.toUpperCase() ?? null) ||
          (p[<string>criteria.field3]?.toString().toUpperCase() ?? null) == (criteria.field3Value?.toUpperCase() ?? null) ||
          (p[<string>criteria.field4]?.toString().toUpperCase() ?? null) == (criteria.field4Value?.toUpperCase() ?? null)
        );
        break;
      }
      case 44: { // all matching
        filteredData = existingData.filter((p: any) =>
          (p[<string>criteria.field1]?.toString().toUpperCase() ?? null) == (criteria.field1Value?.toUpperCase() ?? null) &&
          (p[<string>criteria.field2]?.toString().toUpperCase() ?? null) == (criteria.field2Value?.toUpperCase() ?? null) &&
          (p[<string>criteria.field3]?.toString().toUpperCase() ?? null) == (criteria.field3Value?.toUpperCase() ?? null) &&
          (p[<string>criteria.field4]?.toString().toUpperCase() ?? null) == (criteria.field4Value?.toUpperCase() ?? null)
        );
        break;
      }
    }

    if (filteredData != null && filteredData.length > 0) {
      criteria.status = false;
      criteria.message = TitleCasePipe.prototype.transform(criteria.name) + ' already exists with this criteria => ' + errorCriteria;
    }
    else criteria.status = true;
    return criteria;
  }

  createUniqueValidationObject(name: string, matchingFields: number, pkField: string, pkValue: string, field1: string, field1Value: string,
    field2?: string, field2Value?: string,
    field3?: string, field3Value?: string,
    field4?: string, field4Value?: string) {
    let uniqueValidationCriteria = new UniqueValidationCriteria();
    uniqueValidationCriteria.name = name;
    uniqueValidationCriteria.matchingFields = matchingFields;
    uniqueValidationCriteria.pkField = pkField;
    uniqueValidationCriteria.pkValue = pkValue;

    uniqueValidationCriteria.field1 = field1;
    uniqueValidationCriteria.field1Value = field1Value;

    uniqueValidationCriteria.field2 = field2;
    uniqueValidationCriteria.field2Value = field2Value;

    uniqueValidationCriteria.field3 = field3;
    uniqueValidationCriteria.field3Value = field3Value;

    uniqueValidationCriteria.field4 = field4;
    uniqueValidationCriteria.field4Value = field4Value;
    return uniqueValidationCriteria;
  }


  createInfoObject(name: string, message: string, type?: string) {
    let information = new Information();
    information.name = name;
    information.message = message;
    information.type = type;
    return information;
  }

}
