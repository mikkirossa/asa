import { Injectable } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';
import { concatMap, map, retry, tap } from 'rxjs/operators';
import { UserSettings } from '../models/common/userSettings';
import { Constants } from '../utils/constants';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { environment } from '@environment';
import { ASAPermissionService, PermissionPage, Realm, RealmSearchRequest, Role, RolePage, RoleSearchRequest } from './admin.service';

@Injectable({
  providedIn: 'root'
})
export class CacheService {
  allUsers: Array<any> = new Array<any>();
  allRealms: Array<Realm> = new Array<Realm>();
  _roles: Array<any> = new Array<any>();
  allRoles: Array<any> = new Array<any>();
  _permissions: Array<any> = new Array<any>();
  allPermissions: Array<any> = new Array<any>();

  constructor(private authService: AuthService,
    private asaPermissionService: ASAPermissionService,
    private userService: UserService) {
    
  }

  cacheData() {
    this.authService.getProfile()?.subscribe(result => {
      if (this.authService.getToken(environment.security_api.key) != null) {
        console.log('Cache service start...');

        this.loadAllRealms();
        this.loadAllRolesPermissions();
        setTimeout(() => { this.loadUserSettings(); }, 1000);
      }
    },
      error => {
        this.authService.logout(true)
      }, () => {
        console.log('cache service done...');
      });
  }

  loadSubscriptions(obs: Observable<any>, data: any) {
    obs.subscribe(op => { data = op.items; });
  }

  //getAllApprovalTypes(): Observable<any> {
  //  if (this.allApprovalTypes == undefined || this.allApprovalTypes.length == 0) {
  //    return this.sub_ApprovalTypes;
  //  }
  //  else return of(this.allApprovalTypes);
  //}

  loadAllRealms() {
    const request = new RealmSearchRequest();
    this.asaPermissionService.search_For_Realms(request)
      .subscribe((result: any) => {
          this.allRealms = result.items;
      });
  }

  loadAllRolesPermissions() {
    const self = this;

    if (this.allRealms.length == 0 || this.allRealms == null || this.allRealms == undefined) {
      setTimeout(() => { this.loadAllRolesPermissions(); }, 1000);
    }
    else {

      let obsForRolesArray: Array<Observable<any>> = new Array<Observable<any>>();
      let obsForPermissionsArray: Array<Observable<any>> = new Array<Observable<any>>();

      this.allRealms.forEach((rm) => {
        let request = new RoleSearchRequest();
        request.realmId = rm.id;
        obsForRolesArray.push(self.asaPermissionService.search_For_Roles(request));
        obsForRolesArray.push(self.asaPermissionService.search_For_Permissions(request));
        this.allRoles.push({ 'realmId': rm.id, 'roles': [] });
        this.allPermissions.push({ 'realmId': rm.id, 'roles': [] });
      });


      forkJoin(obsForRolesArray)
        .pipe(
          concatMap(item$ => item$),
        )
        .subscribe((obj: any) => {

          if (obj instanceof RolePage) {

            if (obj.items != null && obj.items.length > 0) {
              let firstRole = obj?.items[0];
              let roleFound = this.allRoles.filter((_r: any) => _r.realmId == firstRole.realmId);
              if (roleFound != null && roleFound.length > 0) {
                roleFound[0].roles = obj?.items;
              }
            }
             
          }

          if (obj instanceof PermissionPage) {

            if (obj.items != null && obj.items.length > 0) {
              let firstPermission = obj?.items[0];
              let permissionFound = this.allPermissions.filter((_r: any) => _r.realmId == firstPermission.realmId);
              if (permissionFound != null && permissionFound.length > 0) {
                permissionFound[0].permissions = obj?.items;
              }
            }

          }


            //let request = new RoleSearchRequest();
            //request.realmId = obj.id;
            //self.asaPermissionService.search_For_Roles(request)
            //  .subscribe((result: any) => {
            //    self._roles.push({ 'realmId': obj.id, 'roles': result.items });
            //    if (self._roles.length == self.allRealms.length)
            //      self.allRoles = self._roles;
            //  });

            //self.asaPermissionService.search_For_Permissions(request)
            //  .subscribe((result: any) => {
            //    self._permissions.push({ 'realmId': obj.id, 'permissions': result.items });
            //    if (self._permissions.length == self.allRealms.length)
            //      self.allPermissions = self._permissions;
            //  });

        },
          error => {
            console.log('Error while fetching All Roles and Permissions.');
          }, () => {
            
            console.log('Done fetching All Roles and Permissions.');
            this.mapPermissionsWithRole();
        });

    }
  }

  mapPermissionsWithRole() {
    const self = this;
    this.allRoles.forEach((r) => {

      r.roles?.forEach((er: any) => {
        er.permissionIds?.forEach(function(pId: any, _ind: number) {

          let _reamsPermissionsFound = self.allPermissions.filter((rp: any) => rp.realmId == r.realmId);
          if (_reamsPermissionsFound != null && _reamsPermissionsFound != undefined && _reamsPermissionsFound.length > 0) {
            _reamsPermissionsFound.forEach((rp) => {
              let _permissionsFound = rp.permissions?.filter((p: any) => p.id == pId);

              if (_permissionsFound != null && _permissionsFound != undefined && _permissionsFound.length > 0) {
                er.permissionIds[_ind] = _permissionsFound[0];
              }
            });
          }

        });

      });
    });
    console.log('All Roles With Permissions', this.allRoles);
  }
  
  loadAllUsers() {
    if (localStorage.getItem('allUsers') == undefined || localStorage.getItem('allUsers') == '') {
      //this.userService.searchUsers()
      //  .pipe(map((p: any) => p.items))
      //  .subscribe((op: any) => {
      //    op?.forEach(p => {
      //      this.allUsers.push({ u: p.userName, f: p.firstName, l: p.lastName });
      //    });
      //    localStorage.setItem('allUsers', JSON.stringify(this.allUsers));
      //  });
    }
  }

  loadUserSettings() {
    if (localStorage.getItem(Constants.User_Settings) == undefined || localStorage.getItem(Constants.User_Settings) == '') {
      this.setUserSettings();
    }
  }
  setUserSettings() {
    if (this.authService.loginName == undefined || this.authService.loginName == null || this.authService.loginName == '')
      setTimeout(() => { this.setUserSettings(); }, 1000);
    else {
      let settings = new UserSettings();
      settings.userName = <string>this.authService.loginName;
      settings.settings = {};
      //localStorage.setItem(Constants.User_Settings, JSON.stringify(settings));
      //this.userService.getUserSettings(<string>this.authService.loginName)
      //  .pipe(retry(5))
      //  .subscribe(op => {
      //    let settings = new UserSettings();
      //    settings.userName = <string>this.authService.loginName;
      //    settings.settings = op[<any>this.authService.loginName];
      //    localStorage.setItem(Constants.User_Settings, JSON.stringify(settings));
      //  });
    }
  }
  getUserSettings() {
    return JSON.parse(<string>localStorage.getItem(Constants.User_Settings));
  }

  getUserNameFromSettings() {
    return JSON.parse(<string>localStorage.getItem(Constants.LoginName));
  }

  getAllUsers(): any {
    const value = localStorage.getItem('allUsers');
    if (value != null) {
      return JSON.parse(value);
    }
  }

  getUser(u: string): any {
    if (u == null || u == undefined) return null;
    const allUsers = localStorage.getItem('allUsers');
    if (allUsers != null) {
      let filteredUsers = JSON.parse(allUsers)?.filter((p: any) => p.u == u);
      if (filteredUsers != null && filteredUsers.length > 0)
        return filteredUsers[0];
    }
    return {l: u, f: ''};
  }

  reloadObject(componentKey: string) {
  }
}
