import {formatDate} from '@angular/common';
import {Inject, Injectable, LOCALE_ID} from '@angular/core';
import {Observable, of} from 'rxjs';
import {ActionButtonsComponent} from '../grid/action-buttons/action-buttons.component';
import { CheckboxComponent } from '../grid/checkbox/checkbox.component';
import {DropdownComponent} from '../grid/dropdown/dropdown.component';
import {SelectedRolesComponent} from '../grid/selected-roles/selected-roles.component';
import {ViewUserPermissionsComponent} from '../grid/view-permissions/view-user-permissions.component';
import {Constants} from '../utils/constants';
import {iCons} from '../utils/icons';
import {CacheService} from './cache.service';
import {CommonService} from './common.service';
import {StatusComponent} from "../grid/status/status.component";

@Injectable({
  providedIn: 'root'
})
export class GridColumnService {
  defaultColDef = {
    resizable: Constants.gridDefaultResizable,
    sortable: Constants.gridDefaultSortable,
    filter: Constants.gridDefaultFilter
  };
  text_NA: any = {
    cellRenderer: (data: any) => {
      return this.getNA(data.value);
    }
  };
  created: any = {
    headerName: 'Created',
    field: 'createDate',
    minWidth: Constants.gridDefaultMinColWidth,
    cellRenderer: (data: any) => {
      return this.getDate(data);
    },
    hide: true
  };
  createdBy: any = {
    headerName: 'Created By',
    field: 'createdBy',
    minWidth: Constants.gridDefaultMinColWidth,
    cellRenderer: (data: any) => {
      return this.getFullUserName(data);
    },
    hide: true
  };
  updated: any = {
    headerName: 'Updated',
    field: 'updateDate',
    minWidth: Constants.gridDefaultMinColWidth,
    cellRenderer: (data: any) => {
      return this.getDate(data);
    },
    hide: true
  };
  updatedBy: any = {
    headerName: 'Updated By',
    field: 'updatedBy',
    minWidth: Constants.gridDefaultMinColWidth,
    cellRenderer: (data: any) => {
      return this.getFullUserName(data);
    },
    hide: true
  };
  actions: any = {
    headerName: 'Actions', field: 'actions', minWidth: Constants.gridDefaultMinColWidth,
    cellRendererFramework: ActionButtonsComponent
  };

  rolesDropDown: any = {
    headerName: 'Select Roles', field: 'roles', minWidth: Constants.gridDefaultMinColWidth,
    cellRendererFramework: DropdownComponent
  };

  selectedRoles: any = {
    headerName: 'Selected Roles', field: 'roles', minWidth: Constants.gridDefaultMinColWidth,
    cellRendererFramework: SelectedRolesComponent
  };

  viewRealmPermissions: any = {
    headerName: 'Permissions', field: 'roles',
    cellRendererFramework: ViewUserPermissionsComponent
  };

  status: any = {
    headerName: 'Status', field: 'roles',
    cellRendererFramework: StatusComponent
  }

  checkBox: any = {
    headerName: '', field: '', width: Constants.gridDefaultCheckboxColWidth,
    cellRendererFramework: CheckboxComponent
  };

  icons = {
    sortAscending: iCons.gridSortAscendingIcon,
    sortDescending: iCons.gridSortDescendingIcon,
  };

  constructor(@Inject(LOCALE_ID) public locale: string,
              private commonService: CommonService,
              private cacheService: CacheService) {
  }

  getColDefinitions(colKey: string, gridComp: any): Observable<any> {
    let gridColumnDefs: any = [];
    if (colKey == Constants.ComponentKeySearchUsers || colKey == Constants.ComponentKeySearchUsersEditRealm)
      gridColumnDefs = [
        {headerName: 'User Name', field: 'userName', resizable: true, minWidth: Constants.gridDefaultMinColWidth},
        { headerName: 'First Name', field: 'firstName', resizable: true, minWidth: Constants.gridDefaultMinColWidth},
        {headerName: 'Last Name', field: 'lastName', resizable: true, minWidth: Constants.gridDefaultMinColWidth},
        {
          headerName: 'Email Address',
          field: 'emailAddress',
          resizable: true,
          minWidth: Constants.gridDefaultMinColWidth
        },
        { headerName: 'User Storage', field: 'userStorage', resizable: true, minWidth: Constants.gridDefaultMinColWidth },
        {
          headerName: 'Is Active',
          field: 'isActive',
          minWidth: Constants.gridDefaultMinColWidth,
          cellRenderer: (data: any) => {
            return '<span class="' + (data.value == true ? 'grid-active-col-icon' : 'grid-in-active-col-icon') + '">' + iCons.activeCircleIcon + '</span>';
          }
        },
        {
          ...this.actions,
          cellRendererParams: {
            permissions: {},
            editable: true,
            deletable: true,
            editRouteURL: 'users/_KEY_/edit',
            componentKey: Constants.ID_User,
            deleteFunction: colKey == Constants.ComponentKeySearchUsers ? 'delete_User' : (colKey == Constants.ComponentKeySearchUsersEditRealm ? 'remove_User_From_Realm' : ''),
            service: gridComp['asaPermissionService'],
            primaryKey: colKey == Constants.ComponentKeySearchUsers ? 'id' : (colKey == Constants.ComponentKeySearchUsersEditRealm ? 'id-realmId' : ''),
            secondaryKey: 'userName',
            primaryKeyHeader: 'User Id',
            secondaryKeyHeader: 'User Name'
          }
        },
      ];
    else if (colKey == Constants.ComponentKeySearchClients || colKey == Constants.ComponentKeySearchClientsEditRealm)
      gridColumnDefs = [

        {headerName: 'Client Name', field: 'clientName', resizable: true, minWidth: Constants.gridDefaultMinColWidth},
        {headerName: 'Client ID', field: 'clientId', resizable: true, minWidth: Constants.gridDefaultMinColWidth},
        {
          headerName: 'Is Active',
          field: 'isActive',
          minWidth: Constants.gridDefaultMinColWidth,
          cellRenderer: (data: any) => {
            return data.value == true ? '<span class="grid-active-col-icon">' + iCons.activeCircleIcon + '</span>' : '';
          }
        },
        {
          ...this.actions,
          cellRendererParams: {
            permissions: {},
            editable: true,
            deletable: true,
            editRouteURL: 'clients/_KEY_/edit',
            componentKey: Constants.ID_Client,
            deleteFunction: colKey == Constants.ComponentKeySearchClients ? 'delete_Client' : (colKey == Constants.ComponentKeySearchClientsEditRealm ? 'remove_Client_From_Realm' : ''),
            service: gridComp['asaPermissionService'],
            primaryKey: colKey == Constants.ComponentKeySearchClients ? 'id' : (colKey == Constants.ComponentKeySearchClientsEditRealm ? 'id-realmId' : ''),
            secondaryKey: 'clientName',
            primaryKeyHeader: 'Client Id',
            secondaryKeyHeader: 'Client Name'
          }
        },
      ];
    else if (colKey == Constants.ComponentKeyUsersRealms)
      gridColumnDefs = [
        {headerName: 'Realm', field: 'name', resizable: true, width: 100},
        {
          ...this.rolesDropDown, width: 100,
          cellRendererParams: {
            applyOption: true,
            doNotshowTitle: true,
          }
        },
        {
          ...this.selectedRoles, autoHeight: true,
          cellRendererParams: {
            permissions: {},
          }
        },
        {
          ...this.viewRealmPermissions, width: 50,
          cellRendererParams: {
            permissions: {},
          }
        },
        {
          ...this.status, width: 70,
          cellRendererParams: {
            permissions: {},
          }
        },
      ];
    else if (colKey == Constants.ComponentKeyClientsRealms)
      gridColumnDefs = [
        {headerName: 'Realm', field: 'name', resizable: true, width: 100},
        {
          ...this.rolesDropDown, width: 100,
          cellRendererParams: {
            applyOption: true,
            doNotshowTitle: true,
          }
        },
        {
          ...this.selectedRoles, autoHeight: true,
          cellRendererParams: {
            permissions: {},
          }
        },
        {
          ...this.viewRealmPermissions, width: 50,
          cellRendererParams: {
            permissions: {},
          }
        },
        {
          headerName: 'Status', field: 'roles', width: 70, cellRenderer: (data: any) => {
            return '<span class="' + (data.value && data.value.length > 0 ? 'grid-active-col-icon' : 'grid-in-active-col-icon') + '">' + iCons.activeCircleIcon + '</span>';
          }
        },
      ];
    else if (colKey == Constants.ComponentKeySearchRoles || colKey == Constants.ComponentKeySearchRolesEditRealm)
      gridColumnDefs = [
        { headerName: 'Name', field: 'name', resizable: true, minWidth: Constants.gridDefaultMinColWidth },
        { headerName: 'Key', field: 'key', resizable: true, minWidth: Constants.gridDefaultMinColWidth },
        { headerName: 'Description', field: 'description', resizable: true, minWidth: Constants.gridDefaultMinColWidth },
        {
          ...this.actions,
          cellRendererParams: {
            permissions: {},
            editable: true,
            editPopup: colKey == Constants.ComponentKeySearchRolesEditRealm ? true : false,
            deletable: true,
            editRouteURL: 'roles/_KEY_/edit',
            componentKey: Constants.ID_Role,
            deleteFunction: colKey == Constants.ComponentKeySearchRoles ? 'delete_Role' : (colKey == Constants.ComponentKeySearchRolesEditRealm ? 'remove_Role_From_Realm' : ''),
            service: gridComp['asaPermissionService'],
            primaryKey: colKey == Constants.ComponentKeySearchRoles ? 'id' : (colKey == Constants.ComponentKeySearchRolesEditRealm ? 'id-realmId' : ''),
            secondaryKey: 'key',
            primaryKeyHeader: 'Role Id',
            secondaryKeyHeader: 'Key'
          }
        },
      ];
    else if (colKey == Constants.ComponentKeySearchPermissions || colKey == Constants.ComponentKeySearchPermissionsEditRealm)
      gridColumnDefs = [
        { headerName: 'Name', field: 'name', resizable: true, minWidth: Constants.gridDefaultMinColWidth },
        { headerName: 'Key', field: 'key', resizable: true, minWidth: Constants.gridDefaultMinColWidth },
        { headerName: 'Description', field: 'description', resizable: true, minWidth: Constants.gridDefaultMinColWidth },
        {
          ...this.actions,
          cellRendererParams: {
            permissions: {},
            editable: true,
            deletable: true,
            editRouteURL: 'users/_KEY_/edit',
            componentKey: Constants.ID_Permission,
            deleteFunction: 'delete_Permission',
            service: gridComp['asaPermissionService'],
            primaryKey: 'id',
            secondaryKey: 'key',
            primaryKeyHeader: 'Permission Id',
            secondaryKeyHeader: 'Key'
          }
        },
      ];
    else if (colKey == Constants.ComponentKeyAddUsersToRealm)
      gridColumnDefs = [
        this.checkBox,
        { headerName: 'First Name', field: 'firstName&lastName', resizable: true, minWidth: Constants.gridDefaultMinColWidth, cellRenderer: (data: any) => { return this.getFullName(data); }, },
        {
          ...this.rolesDropDown, width: 100,
          cellRendererParams: {
              applyOption: false,
              doNotshowTitle: false,            
          }
        }
      ];
    else if (colKey == Constants.ComponentKeyAddClientsToRealm)
      gridColumnDefs = [
        this.checkBox,
        { headerName: 'Client Name', field: 'firstName&lastName', resizable: true, minWidth: Constants.gridDefaultMinColWidth, cellRenderer: (data: any) => { return this.getClientName(data); }, },
        {
          ...this.rolesDropDown, width: 100,
          cellRendererParams: {
            permissions: {},
          }
        }
      ];
    else if (colKey == Constants.ComponentKeyEditRolesInRealmAddPermissions)
      gridColumnDefs = [
        this.checkBox,
        { headerName: 'Name', field: 'name', resizable: true, minWidth: Constants.gridDefaultMinColWidth },
        { headerName: 'Key', field: 'key', resizable: true, minWidth: Constants.gridDefaultMinColWidth },
        { headerName: 'Description', field: 'description', resizable: true, minWidth: Constants.gridDefaultMinColWidth },
      ];
    return of(gridColumnDefs);
  }


  getDefaultSortColumn(colKey: string): any {
    let sortModel: any = { colId: '', sort: 'asc' }
    if (colKey == Constants.ComponentKeySearchUsers)
      sortModel = { colId: 'firstName', sort: 'asc' }
    else if (colKey == Constants.ComponentKeySearchClients)
      sortModel = { colId: 'clientName', sort: 'asc' }
    else if (colKey == Constants.ComponentKeyUsersRealms)
      sortModel = { colId: 'name', sort: 'asc' }
    else if (colKey == Constants.ComponentKeyClientsRealms)
      sortModel = { colId: 'name', sort: 'asc' }
    else if (colKey == Constants.ComponentKeySearchRoles)
      sortModel = { colId: 'name', sort: 'asc' }
    else if (colKey == Constants.ComponentKeySearchPermissions)
      sortModel = { colId: 'key', sort: 'asc' }
      return sortModel;
  }

  getNA(data: any): any {
    if (data == null || data == undefined || data == '')
      return Constants.TEXT_NA;
    return data;
  }

  getFullUserName(data: any) {
    let _u = this.cacheService.getUser(data.value);
    return _u != null ? _u?.l + (_u?.f != '' ? ', ' + _u?.f : '') : '';
  }

  getDate(data: any) {
    if (data == null || data.value == null) return '';
    return formatDate(data.value, Constants.AppDateFormat, this.locale);
    //return formatDate(new Date(data.value + ' UTC'), Constants.AppDateFormat, this.locale);
  }

  getFullName(obj: any) {
    return obj.data.firstName + ' ' + obj.data.lastName;
  }

  getClientName(obj: any) {
    return obj.data.clientName;
  }
}
