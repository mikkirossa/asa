import { Component } from '@angular/core';
import { environment } from '@environment';
import { mergeMap as _observableMergeMap, catchError as _observableCatch } from 'rxjs/operators';
import { Observable, from as _observableFrom, throwError as _observableThrow, of as _observableOf } from 'rxjs';
import { Injectable, Inject, Optional, InjectionToken } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpResponseBase } from '@angular/common/http';

export class ApiServiceBase {
    public transformOptions(options: any) : any{
        return _observableOf(options);
    }

  public getBaseUrl(url: string): string{
    console.log(environment.security_api.app_url);
        return environment.security_api.app_url;
    }
}
