import {
  HttpEvent,
  HttpHandler,
  HttpRequest,
  HttpErrorResponse,
  HttpInterceptor
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Constants } from '../utils/constants';
import { ErrorService } from './error.service';
import { JwtInterceptor } from './jwt.interceptor';
import { LoaderService } from './loader.service';
import { UserService } from './user.service';

@Injectable()
export class ErrorIntercept implements HttpInterceptor {
  constructor(private errorService: ErrorService,
    private userService: UserService,
    private loaderService: LoaderService ) { }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          this.loaderService.isLoading.next(false);
          //this.jwtInterceptor.removeRequest(request);

          let errorMessage = '';
          // check if token is invalid
          if ((<string>error.url)?.toUpperCase()?.indexOf('VALIDATE') > -1 || (<string>error.url)?.toUpperCase()?.indexOf('TOKEN') > -1) {
            console.log('JWT is invalid...');
            return new Promise<any>((resolve, reject) => {
              reject(error);
            });
          }
          else {
            if (error.error instanceof Blob && error.error.type === "application/json") {
              return new Promise<any>((resolve, reject) => {
                let reader = new FileReader();
                reader.onload = (e: Event) => {
                  try {
                    const errmsg = JSON.parse((<any>e.target).result);
                    //reject(new HttpErrorResponse({
                    //  error: errmsg,
                    //  headers: error.headers,
                    //  status: error.status,
                    //  statusText: error.statusText,
                    //  url: <string>error.url
                    //}));

                    // check if token is invalid
                    //if (error.url?.toUpperCase()?.indexOf(''))
                    if (errmsg && errmsg["errmsg"] && errmsg["errmsg"] != undefined)
                      this.showErrorMessage(errmsg.Message);
                    else
                      this.showErrorMessage(errmsg);

                    return throwError(errmsg);
                  } catch (e) {
                    console.log(e);
                    reject(error);
                    return throwError(error);
                  }
                };
                reader.onerror = (e) => {
                  console.log(e);
                  reject(error);
                };
                reader.readAsText(error.error);
              });
            }
            else if (error.error instanceof ErrorEvent) {
              // client-side error
              errorMessage = `Error: ${error.error.message}`;
            } else {
              // server-side error
              errorMessage = `Error Status: ${error.status}\nMessage: ${error.message}`;
            }
            console.log(errorMessage);

            this.showErrorMessage(errorMessage);
          
            return throwError(errorMessage);
          }
        })
      )
  }

  showErrorMessage(errorMessage: string) {

    console.log('Error from Service - ', errorMessage);

    if (this.userService.getUserPermissions() != null && errorMessage != undefined && errorMessage != '') // user is logged in then show error message on  top
    {
      let _e = { type: Constants.ERROR_KEY.toUpperCase(), message: errorMessage }
      this.errorService.createErrorInfoSuccessObject([_e]);
    }
  }
}
