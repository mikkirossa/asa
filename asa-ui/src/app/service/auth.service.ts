import { Injectable } from '@angular/core';
import { JsonWebToken, ASAPermissionService, ProfileRequest, AsaCredentials, PrincipalProfile } from './admin.service';
import { Router } from '@angular/router';
import { environment } from '@environment';
//import Swal from 'sweetalert2';
import { TitleService } from './title.service';
import { Observable } from 'rxjs';
import { Constants } from '../utils/constants';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  USER_KEYS: any = { sec: environment.security_api.userKey };
  GRANT_TYPE = 'password';
  loginName: string | undefined;
  attempt: number = 0;
  constructor(private router: Router,
    private asaPermissionService: ASAPermissionService,
    private titleService: TitleService) { }

  validateToken(redirect: boolean) {
    const token = this.getToken(environment.security_api.key);
    const self = this;

    if (token == null) {
      this.logout(redirect);
    } else {
      this.loadProfile();
    }
  }

  getProfile(): Observable<PrincipalProfile> {
    const request = new ProfileRequest();
    const self = this;
    const token = this.getToken(environment.security_api.key);

    if (token == null) {
      return null as any;
    }

    request.client_id = environment.security_api.client_id;
    request.client_secret = environment.security_api.client_secret;
    request.token = token.access_token;

    return this.asaPermissionService.get_Profile(request);
  }

  loadProfile() {
    const self = this;
    this.getProfile()?.subscribe(result => {
      console.log('producing profile:', result);
      localStorage.setItem(environment.security_api.userId, <string>result.id);
      this.loginName = result.loginName;
      self.titleService.updateProfile(result);
      localStorage.setItem(Constants.Permission_Keys, JSON.stringify(result.permissionKeys));

      localStorage.setItem(Constants.User_Settings, JSON.stringify((<any>result)?.settings));

      localStorage.setItem(Constants.LoginName, <string>result.loginName);
    },
      error => self.logout(true));
  }

  getToken(key: string): JsonWebToken {
    const value = localStorage.getItem(this.USER_KEYS[key]);

    if (value != null) {
      return JsonWebToken.fromJS(JSON.parse(value));
    }

    return null as any;
  }

  login(username: string, password: string) {
    const self = this;
    const credentials = new AsaCredentials();

    credentials.username = username;
    credentials.password = password;
    credentials.client_id = environment.security_api.client_id;
    credentials.client_secret = environment.security_api.client_secret;
    credentials.grant_type = this.GRANT_TYPE;
    self.attempt++;
    this.asaPermissionService.create_Token(credentials)
      .subscribe({
        next(result) {
          const stringified = JSON.stringify(result);
          localStorage.setItem(self.USER_KEYS[environment.security_api.key], stringified);
          self.loadProfile();
          self.router.navigate(['users/search']);
        },
        error(prop) {
          console.log(prop);
          self.router.navigate(['login'], { queryParams: { 'error': 'true', 'attempt': (self.attempt - 1).toString() } });

        }
      });
  }

  logout(redirect: boolean) {
    const self = this;
    localStorage.removeItem(self.USER_KEYS[environment.security_api.key]);
    localStorage.removeItem(Constants.User_Settings);
    localStorage.removeItem(Constants.Permission_Keys);
    if (redirect) {
      self.router.navigate(['login']);
    }
  }
}
