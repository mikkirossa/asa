import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { PrincipalProfile } from './admin.service';

@Injectable()
export class TitleService {

    private titleMessage = new BehaviorSubject('title');
    observableTitle = this.titleMessage.asObservable();

    private profileMessage = new BehaviorSubject<PrincipalProfile>(new PrincipalProfile());
    observableProfile = this.profileMessage.asObservable();

    constructor() {}

    getTitleSubscription() {
        return this.observableTitle;
    }

    updateTitle(title: string) {
        this.titleMessage.next(title);
    }

    getProfileSubscription() {
        return this.observableProfile;
    }

    updateProfile(profile: PrincipalProfile) {
        this.profileMessage.next(profile);
    }

}
