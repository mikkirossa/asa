import { Injectable } from '@angular/core';
import { Message } from '../models/common/validationCriteria';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  constructor(private commonService: CommonService) { }

  createErrorInfoSuccessObject(items: any) { // items can be Array of Exception, info, success messages

    let existingMessages = this.commonService.errorInfoSuccessMessageSubject.getValue()
    console.log('existingMessages', existingMessages);

    if (existingMessages != null && existingMessages.items != null && existingMessages.items.length > 0) {
      let _message = existingMessages;
      let _msg: string = existingMessages.message;
      items.forEach((p: any) => {
        _msg += p.message
        _message.items.push(p);
      });
     
      _message.message = _msg;
      this.commonService.setErrorInfoSuccessMessageSubject(_message);
    }
    else {
      let _message = new Message();
      let _msg: string = '';
      items.forEach((p: any) => {
        _msg += p.message
      });
      _message.items = items;
      _message.message = _msg;
      this.commonService.setErrorInfoSuccessMessageSubject(_message);
    }
      

    

   
  }
}
