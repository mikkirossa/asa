import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Constants } from '../utils/constants';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  private editRealmFlagSubject = new Subject<any>();
  private editRealmGridRefreshSubject = new Subject<any>();

  private gridRowSubject = new Subject<any>();
  private newGridRowSubject = new Subject<any>();
  private deletedGridRowSubject = new Subject<any>();
  public errorInfoSuccessMessageSubject = new BehaviorSubject<any>(null);
  public validationMessages: any;
  constructor() {
    this.validationMessages = new Constants.validation();
  }

  setGridRowSubject(message: any) {
    this.gridRowSubject.next(message);
  }

  clearGridRowSubject() {
    this.gridRowSubject.next(true);
  }

  getGridRowSubject(): Observable<any> {
    return this.gridRowSubject.asObservable();
  }
  
  setNewGridRowSubject(message: any) {
    this.newGridRowSubject.next(message);
  }

  clearNewGridRowSubject() {
    this.newGridRowSubject.next(true);
  }

  getNewGridRowSubject(): Observable<any> {
    return this.newGridRowSubject.asObservable();
  }

  setDeletedGridRowSubject(message: any) {
    this.deletedGridRowSubject.next(message);
  }

  clearDeletedGridRowSubject() {
    this.deletedGridRowSubject.next(true);
  }

  getDeletedGridRowSubject(): Observable<any> {
    return this.deletedGridRowSubject.asObservable();
  }

  replaceUndefinied(item: any): any {
    var str = JSON.stringify(item, function (key, value) { return (value === undefined) ? "" : value });
    return JSON.parse(str);
  }

  setErrorInfoSuccessMessageSubject(message: any) {
    this.errorInfoSuccessMessageSubject.next(message);
  }

  clearErrorInfoSuccessMessageSubject() {
    this.errorInfoSuccessMessageSubject.next(null);
  }

  getErrorInfoSuccessMessageSubject(): Observable<any> {
    return this.errorInfoSuccessMessageSubject.asObservable();
  }

  setEditRealmFlagSubject(message: any) {
    this.editRealmFlagSubject.next(message);
  }

  clearEditRealmFlagSubject() {
    this.editRealmFlagSubject.next(true);
  }

  getEditRealmFlagSubject(): Observable<any> {
    return this.editRealmFlagSubject.asObservable();
  }

  setEditRealmGridRefreshSubject(message: any) {
    this.editRealmFlagSubject.next(message);
  }

  clearEditRealmGridRefreshSubject() {
    this.editRealmFlagSubject.next(true);
  }

  getEditRealmGridRefreshSubject(): Observable<any> {
    return this.editRealmFlagSubject.asObservable();
  }
}
