import {Component, OnInit} from "@angular/core";
import {iCons} from "../../utils/icons";
import {CommonService} from "../../service/common.service";
import {Constants} from "../../utils/constants";

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html'
})

export class StatusComponent implements OnInit {
  params: any
  activeCircle = iCons.activeCircleIcon;

  constructor(private commonService: CommonService) {
  }

  ngOnInit() {
  }

  agInit(params: any) {
    this.params = params;
  }

}
