import { Component, OnInit } from '@angular/core';
import { ASAPermissionService } from '../../service/admin.service';
import { CacheService } from '../../service/cache.service';
import { CommonService } from '../../service/common.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-selected-roles',
  templateUrl: './selected-roles.component.html',
  styleUrls: ['./selected-roles.component.css']
})
export class SelectedRolesComponent implements OnInit {
  params: any;
  addClientOrUserKey: any;
  constructor(private commonService: CommonService,
    private asaPermissionService: ASAPermissionService,
    public cacheService: CacheService) {
    this.addClientOrUserKey = new Constants.addClientOrUserKey();
  }

  agInit(params: any) {
    this.params = params;
  }

  ngOnInit() { }

  removeRole(roleId: string) {
    if (this.params.data.addClientOrUserKey == this.addClientOrUserKey.user) {
      this.asaPermissionService.remove_User_From_Role(this.params.data.selectedUserId, this.params.data.id, roleId)
        .subscribe((result: any) => {
          this.params.data.roles = this.params.data.roles.filter((p: any) => p.roleId != roleId);
        });
    }
    else if (this.params.data.addClientOrUserKey == this.addClientOrUserKey.client) {
      this.asaPermissionService.remove_Client_From_Role(this.params.data.selectedClientId, this.params.data.id, roleId)
        .subscribe((result: any) => {
          this.params.data.roles = this.params.data.roles.filter((p: any) => p.roleId != roleId);
        });
    }
  }

  alignItems(el: any) {
    let _rolesWidth: number = 0;
    let _mainDivWidth: number = 0;
    var mainDiv = el.closest(".ag-cell-auto-height");
    _mainDivWidth = Number(mainDiv?.style.width.replace('px', ''));

    var _roles = el.childNodes[0].children;
    if (_roles != null && _roles.length > 0) {
      for (var i = 0, len = _roles.length; i < len; i++) {
        _rolesWidth += _roles[i]?.offsetWidth;
      }
    }

    if (_rolesWidth != null && _rolesWidth != undefined && _rolesWidth >= 0 && _mainDivWidth != null && _mainDivWidth != undefined && _mainDivWidth >= 0 && ((_rolesWidth + 20) < _mainDivWidth)) {
        el.childNodes[0].style.setProperty('padding-top', '18px');
    }
    else {
      el.childNodes[0].style.setProperty('padding-top', '0px');
    }
    //console.log(el, _rolesWidth, _mainDivWidth);
    return el;
  }
}
