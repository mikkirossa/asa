import {Component, OnInit} from '@angular/core';
import {CacheService} from '../../service/cache.service';
import {CommonService} from '../../service/common.service';
import {Constants} from '../../utils/constants';

@Component({
  selector: 'app-view-user-permissions',
  templateUrl: './view-user-permissions.component.html',
  styleUrls: ['./view-user-permissions.component.css']
})
export class ViewUserPermissionsComponent implements OnInit {
  params: any;
  const: any = Constants;

  constructor(private commonService: CommonService,
              public cacheService: CacheService) {
  }

  agInit(params: any) {
    this.params = params;
  }

  ngOnInit() {
  }

  viewPermissions(colDefKey: string) {
    this.params.componentKey = colDefKey;
    this.commonService.setGridRowSubject(this.params);
  }
}
