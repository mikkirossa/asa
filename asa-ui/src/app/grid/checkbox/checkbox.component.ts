import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../service/common.service';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {
  params: any;
  constructor(private commonService: CommonService) { }

  ngOnInit(): void {
  }

  agInit(params: any) {
    this.params = params;
  }
}
