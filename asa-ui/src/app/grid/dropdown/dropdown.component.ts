import { Component, OnChanges, OnInit } from '@angular/core';
import { ASAPermissionService } from '../../service/admin.service';
import { CacheService } from '../../service/cache.service';
import { CommonService } from '../../service/common.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit, OnChanges {
  params: any;
  selectedRole: Array<string> = new Array<string>();
  addClientOrUserKey: any;
  constructor(private commonService: CommonService,
    private asaPermissionService: ASAPermissionService,
    public cacheService: CacheService) {
    this.addClientOrUserKey = new Constants.addClientOrUserKey();
  }

  agInit(params: any) {
    this.params = params;
    this.addDefaultElement();
  }

  addDefaultElement() {
    if (this.params.data?.roles == null || this.params.data?.roles?.length == 0)
      this.selectedRole.push('Select Multiple');
    else if (this.params.data?.roles != null && this.params.data?.roles?.length > 0)
      this.selectedRole.push('Select More');
  }

  ngOnInit() {
  }

  ngOnChanges() {
    console.log('ng change');
    this.addDefaultElement();
  }

  addRoles() {
    this.selectedRole.forEach((p: string) => {
      if (p?.trim() != "" && p.trim() != 'Select Multiple' && p.trim() != 'Select More') {
        this.addUserToRole(p);
      }
    });
    setTimeout(() => { this.selectedRole = new Array<string>(); this.addDefaultElement(); }, 500);
  }


  addUserToRole(singleRole: string) {
    console.log(singleRole);
    if (this.params.data.addClientOrUserKey == this.addClientOrUserKey.user) {
      this.asaPermissionService.add_User_To_Role(this.params.data.selectedUserId, this.params.data.id, singleRole.split('-')[0])
        .subscribe((result: any) => {
          if (this.params.data.roles == undefined || this.params.data.roles == null) this.params.data.roles = new Array<any>();
          this.params.data.roles.push({ 'roleId': singleRole.split('-')[0], 'role': singleRole.split('-')[1] });
        });
    }
    else if (this.params.data.addClientOrUserKey == this.addClientOrUserKey.client) {
      this.asaPermissionService.add_Client_To_Role(this.params.data.selectedClientId, this.params.data.id, singleRole.split('-')[0])
        .subscribe((result: any) => {
          if (this.params.data.roles == undefined || this.params.data.roles == null) this.params.data.roles = new Array<any>();
          this.params.data.roles.push({ 'roleId': singleRole.split('-')[0], 'role': singleRole.split('-')[1] });
        });
    }
  }

  isAlreadySelected(rId: string) {
    this.addDefaultElement();
    let _existingRole = this.params.data?.roles?.filter((p: any) => p.roleId == rId);
    if (_existingRole && _existingRole != undefined && _existingRole.length > 0)
      return 'display-none';
    else
      return '';
  }

  updateSelectedItems() {
    console.log('this.selectedRole', this.selectedRole);
    this.params.data.selectedRoles = JSON.parse(JSON.stringify(this.selectedRole));
  }
 
}
