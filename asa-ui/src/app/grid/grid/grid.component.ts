import {Component, ElementRef, Input, OnChanges, OnInit, ViewChild} from '@angular/core';
import {Subscription} from 'rxjs';
import { ASAPermissionService } from '../../service/admin.service';
import {CacheService} from '../../service/cache.service';
import {CommonService} from '../../service/common.service';
import {GridColumnService} from '../../service/grid-column.service';
import { LoaderService } from '../../service/loader.service';
import { MyLoaderComponent } from '../../shared/my-loader/my-loader.component';
import {Constants} from '../../utils/constants';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit, OnChanges {
  subscription: Subscription | undefined;
  @Input() fetching!: number;
  @Input() gridData: any = [];
  @Input() colDefKey: any;
  @Input() enableShowHideCols: boolean = false;
  @Input() isReadOnly: boolean = false;
  @Input() headerHeight: number = 30;
  @ViewChild('ddlColList')
  ddlColList!: ElementRef;
  @ViewChild('ddlColListHandler')
  ddlColListHandler!: ElementRef;
  domLayout = "autoHeight";
  gridOptions: any;
  gridOptionsArray: any;
  // gridColumnDefs: any;

  gridPageSizeArray = Constants.gridPageSizeArray;
  gridDefaultPageSize = Constants.gridDefaultPageSize;

  loadingCellRenderer: any = MyLoaderComponent;
  loadingMessage = Constants.gridDefaultLoadingMessage;
  oldFeatchingState: number = 0;
  constructor(private commonService: CommonService,
              private cacheService: CacheService,
              private elementRef: ElementRef,
    private gridColumnService: GridColumnService,
    private asaPermissionService: ASAPermissionService,
    private loaderService: LoaderService) {
    this.subscription = this.commonService.getNewGridRowSubject().subscribe(x => {
      if (x.actionKey == Constants.ActionKeyAdded)
        this.addNewRow(x);
      else if (x.actionKey == Constants.ActionKeyUpdated)
        this.updateRow(x);
    });

    this.subscription = this.commonService.getDeletedGridRowSubject().subscribe(x => {
      this.deleteRow(x.data);
    });
  }

  ngOnChanges() {
    if (this.oldFeatchingState != this.fetching) {
      this.oldFeatchingState = this.fetching;
      this.gridOptions?.api?.showLoadingOverlay();
    }
    //this.showHideColumn('actions');
    this.gridOptions?.columnApi?.setColumnVisible('actions', !this.isReadOnly);
    this.gridOptions?.columnApi?.autoSizeAllColumns();
  }

  ngOnInit(): void {
    //console.log('this.colDefKey', this.colDefKey);
    //// add column list in grid options
    //// or pass col defs from parent
    this.gridColumnService.getColDefinitions(this.colDefKey, this)
      .subscribe(p => {
        //this.gridColumnDefs = p
        this.gridOptions = {
          columnDefs: p,// this.gridColumnDefs,
          defaultColDef: this.gridColumnService.defaultColDef,
          suppressCellSelection: true,
          rowSelection: 'single',
          pagination: true,
          paginationPageSize: this.gridDefaultPageSize,
          onGridReady: (params: any) => {
            params.api.sizeColumnsToFit();
            this.onGridReady();
            const sortModel = [
              this.gridColumnService.getDefaultSortColumn(this.colDefKey)
            ];
            this.gridOptions.api?.setSortModel(sortModel);
            //this.gridOptions.api?.showLoadingOverlay();
            //this.gridOptions.api?.showNoRowsOverlay();
          },
          onGridSizeChanged: (params: any) => {
            params.api.sizeColumnsToFit();
          },
          icons: this.gridColumnService.icons,
          //getRowHeight: () => {
          //  return 90;
          //},
          overlayLoadingTemplate: this.loadingMessage,
          //loadingCellRenderer: new MyLoaderComponent(this.loaderService),
          //loadingCellRendererParams: this.loadingCellRendererParams

          onFirstDataRendered: () => {
            console.log("Data loaded in Grid");
          },
          headerHeight: this.headerHeight
        }
      });
  }

  onGridReady() {
  }

  ngAfterViewInit() {
    var paginationDiv = this.elementRef.nativeElement.querySelector('.ag-paging-panel');
    var gridPaginationDdl = this.elementRef.nativeElement.querySelector('.custom-grid-pagination-ddl');
    //paginationDiv.insertAdjacentHTML('beforeend', gridPaginationDdl.outerHTML);
    paginationDiv.insertAdjacentElement('beforeend', gridPaginationDdl);
  }

  changePageSize() {
    this.gridOptions.api.paginationSetPageSize(Number(this.gridDefaultPageSize));
  }

  showHideColumn(col: any): void {
    this.ddlColListHandler.nativeElement.setAttribute('class', 'col-ddl-title nav-link dropdown-toggle show');
    this.ddlColListHandler.nativeElement.setAttribute('aria-expanded', 'true');
    this.ddlColList.nativeElement.setAttribute('class', 'dropdown-menu ddl-ul show')

    let visible = this.gridOptions.columnApi?.getColumn(col).visible;
    this.gridOptions.columnApi?.setColumnVisible(col, !visible);
  }

  addNewRow(rowData: any) {
    console.log('adding new row in grid', rowData);
    this.gridOptions.api?.applyTransaction({
      add: [rowData.data],
      addIndex: 0,
    });
  }

  updateRow(rowData: any) {
    console.log('updating row in grid', rowData);
    this.gridOptions.api?.applyTransaction({
      update: [rowData.node.data],
    });
    //this.gridOptions.api?.refreshRows([rowData.node.data]);
  }

  deleteRow(rowData: any) {
    console.log('deleting row in grid', rowData);
    this.gridOptions.api?.applyTransaction({
      remove: [rowData]
    });
  }
}
