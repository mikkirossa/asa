import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from '../../service/common.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-action-buttons',
  templateUrl: './action-buttons.component.html',
  styleUrls: ['./action-buttons.component.css']
})
export class ActionButtonsComponent implements OnInit {
  params: any;
  constructor(private commonService: CommonService,
    private router: Router  ) { }

  agInit(params: any) {
    this.params = params;
  }

  ngOnInit() { }

  editRow() {
    //let rowData = this.params;
    //let i = rowData.rowIndex;
    //console.log(rowData);

    //rowData.data.baseId = 'AAA';
    //rowData.node.setDataValue('baseId', 'BBB')
    //rowData.refreshCell();


    //this.params.actionKey = Constants.ActionKeyEdit;
    //this.commonService.setGridRowSubject(this.params);
    console.log(this.params);
    if (this.params.editPopup) {
      this.params.actionKey = Constants.ActionKeyEdit;
      this.commonService.setGridRowSubject(this.params);
    } else {
      let editRouteURL = this.params.editRouteURL.replace('_KEY_', this.params.data.id);
      this.router.navigate([editRouteURL]);
    }
  }

  deleteRow() {
    this.params.actionKey = Constants.ActionKeyDelete;
    this.commonService.setGridRowSubject(this.params);
  }

}
