import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../service/common.service';
import { Config } from '../../utils/config';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.css']
})
export class MasterComponent implements OnInit {
  selectedTab: string = 'nav-tab-btn-1'
  permissions: any = Config.Permissions;

  constructor(public commonService: CommonService) {
  }

  ngOnInit(): void {
  }

  tabClicked(e: any) {
    this.selectedTab = e;
  }

  openAddModal(colDefKey: string) {
    let modalData = {
      componentKey: colDefKey,
      actionKey: Constants.ActionKeyNew
    };
    this.commonService.setGridRowSubject(modalData);
  }
}
