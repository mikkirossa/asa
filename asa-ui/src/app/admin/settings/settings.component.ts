import { Component, OnInit } from '@angular/core';
import { Information, Message } from '../../models/common/validationCriteria';
import { UserSettings } from '../../service/admin.service';
import { AuthService } from '../../service/auth.service';
import { CacheService } from '../../service/cache.service';
import { CommonService } from '../../service/common.service';
import { ErrorService } from '../../service/error.service';
import { UserService } from '../../service/user.service';
import { ValidationService } from '../../service/validation.service';
import { Constants } from '../../utils/constants';
import { MasterComponent } from '../master/master.component';
import { environment } from '@environment';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent extends MasterComponent implements OnInit {
  userSettings: any;
  originalUserSettings: any;
  message: string = '';
  constructor(private cacheService: CacheService,
    private userService: UserService,
    private authService: AuthService,
    public commonService: CommonService,
    private validationService: ValidationService,
    private errorService: ErrorService) {
    super(commonService);
    this.userSettings = this.cacheService.getUserSettings();
  }

  ngOnInit(): void {
    this.getUserSettings();
  }

  getUserSettings() {
    this.userSettings = this.userService.getAppSpecificSettings();
    this.originalUserSettings = JSON.parse(JSON.stringify(this.userSettings));
  }

  save() {
    var userSettings: Array<UserSettings> = new Array<UserSettings>();

    let setting = new UserSettings();
    setting.key = Constants.User_SettingsKey;
    setting.settings = this.userSettings;

    userSettings.push(setting);
    this.userService.addUserSetting(<string>localStorage.getItem(environment.security_api.userId), userSettings)
      .subscribe(result => {
        this.message = "User Settings saved successfully.";
        this.authService.validateToken(true);
        this.originalUserSettings = JSON.parse(JSON.stringify(this.userSettings));
      });
  }

  resetForm() {
    this.userSettings = JSON.parse(JSON.stringify(this.originalUserSettings));
    this.message = '';
  }
}
