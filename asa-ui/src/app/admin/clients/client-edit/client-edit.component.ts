import {Component, OnDestroy, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {
  ASAPermissionService,
  ClientPrincipal, RealmSearchRequest,
} from "../../../service/admin.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Constants} from "../../../utils/constants";
import {CacheService} from "../../../service/cache.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.css']
})

export class ClientEditComponent implements OnInit {
  gridDataFetching: number = 0;

  formGroup: FormGroup = this.formBuilder.group({
    clientId: ['', Validators.required],
    clientName: ['', Validators.required],
    clientStorage: ['', Validators.required],
    isActive: [false, Validators.required],
    canSetPassword: [true, Validators.required]
  });

  client!: ClientPrincipal;
  oldClientData!: ClientPrincipal;
  colDefKey = Constants.ComponentKeyClientsRealms;
  itemRowData: ClientPrincipal[] | undefined;
  addClientOrUserKey: any;
  subscription: Subscription = new Subscription();

  constructor(private asaPermissionService: ASAPermissionService,
              private activatedRoute: ActivatedRoute,
              private cacheService: CacheService,
    private formBuilder: FormBuilder) {
    this.addClientOrUserKey = new Constants.addClientOrUserKey();
  }

  ngOnInit() {
    const self = this;
    this.subscription.add(this.activatedRoute.params.subscribe(params => {
      self.asaPermissionService.get_Client(params['principalid'])
        .subscribe((results: any) => {
          console.log(results);
          self.client = results;
          this.oldClientData = JSON.parse(JSON.stringify(this.client));
          this.createForm();
          this.loadRealms();
        })
    }));
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      clientId: [this.client.clientId, Validators.required],
      clientName: [this.client.clientName, Validators.required],
      clientStorage: [this.client.clientStorage, Validators.required],
      isActive: [this.client.isActive, Validators.required],
      canSetPassword: [this.client.canSetPassword, Validators.required]
    });
  }

  loadRealms() {
    if (this.cacheService.allRoles.length == 0 || this.cacheService.allRoles == null || this.cacheService.allRoles == undefined) {
      setTimeout(() => { this.loadRealms(); }, 500);
    }
    else {
      const request = new RealmSearchRequest();
      this.gridDataFetching++;
      this.subscription.add(this.asaPermissionService.search_For_Realms(request)
        .subscribe((result: any) => {
          // console.log(result);
          result.items?.forEach((realm: any) => {
            console.log(realm);
            realm.selectedClientId = this.client.id;
            realm.addClientOrUserKey = this.addClientOrUserKey.client;
            this.client.realms?.forEach((cRealm) => {
              if (realm.id == cRealm.realmId) {
                realm.roles = cRealm.roles;
              }
            });
          });
          console.log('with roles', result.items);
          this.itemRowData = result.items;
        }));
    }
  }

  save() {
    const self = this;
    const client = new ClientPrincipal();

    client.clientId = this.formGroup.controls['clientId'].value;
    client.clientName = this.formGroup.controls['clientName'].value;
    client.clientStorage = this.formGroup.controls['clientStorage'].value;

    console.log(client);

    // this.asaPermissionService.save_Client(client)
    //   .subscribe(result => {
    //
    //       if (result !== null && result !== '') {
    // Swal.fire(
    //   {
    //     title: 'Client Secret Generated!',
    //     text: result,
    //     type: 'success',
    //     confirmButtonClass: 'btn btn-success',
    //     buttonsStyling: false
    //   });
    // } else {
    // Swal.fire(
    //   {
    //     title: 'Client Added!',
    //     text: 'Login with the credentials against your selected storage unit.',
    //     type: 'success',
    //     confirmButtonClass: 'btn btn-success',
    //     buttonsStyling: false
    //   });
    // }

    // self.router.navigate(['/client', 'search']);
    // },
    // error => {
    // Swal.fire(error);
    //       });
  }

  resetSecret() {
    const self = this;
    // Swal.fire({
    //   title: 'Are you sure?',
    //   text: 'Resetting the secret will require updates to any applications using the current ' +
    //     'credentials! If you proceed, the new secret will be returned and will need to be saved.',
    //   type: 'warning',
    //   showCancelButton: true,
    //   confirmButtonClass: 'btn btn-success',
    //   cancelButtonClass: 'btn btn-danger',
    //   confirmButtonText: 'Reset it!',
    //   buttonsStyling: false
    // }).then((result) => {
    //   if (result.value) {
    //     self.asaPermissionService.reset_Client_Secret(self.client.clientId)
    //       .subscribe({
    //         next(secret) {
    //           Swal.fire(
    //             {
    //               title: 'Secret Reset!',
    //               text: secret,
    //               type: 'success',
    //               confirmButtonClass: 'btn btn-success',
    //               buttonsStyling: false
    //             });
    //         }
    //       });
    //   }
    // })
  }

  resetForm() {
    this.client = this.oldClientData;
    this.createForm();
  }

}
