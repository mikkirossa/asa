import {Component, OnDestroy, OnInit} from "@angular/core";
import {
  ClientPrincipal,
  ASAPermissionService, PrincipalStorageSearchRequest, PrincipalStorage, ClientPrincipalSearchRequest,
} from '../../../../../../asa-ui/src/app/service/admin.service';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Constants} from "../../../utils/constants";
import {catchError} from "rxjs/operators";
import {Subscription, throwError} from "rxjs";

@Component({
  selector: 'app-client-add',
  templateUrl: './client-add.component.html',
  styleUrls: ['./client-add.component.css']
})

export class ClientAddComponent implements OnInit, OnDestroy {
  formGroup: FormGroup = new FormGroup({
    clientId: new FormControl('', [Validators.required]),
    clientName: new FormControl('', [Validators.required]),
    clientStorageId: new FormControl('', [Validators.required])
  });

  client!: ClientPrincipal;
  storages: PrincipalStorage[] | undefined;
  selectedStorage: PrincipalStorage | undefined;
  colDefKey = Constants.ComponentKeyUsersRealms;
  itemRowData: ClientPrincipal[] | undefined;
  subscription: Subscription = new Subscription();

  constructor(private asaPermissionService: ASAPermissionService,
              private activatedRoute: ActivatedRoute,
              private formBuilder: FormBuilder,
              private router: Router) {
  }

  ngOnInit() {
    this.client = new ClientPrincipal();
    this.loadStorages();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  loadStorages() {
    const self = this;
    const request = new PrincipalStorageSearchRequest();
    this.subscription.add(this.asaPermissionService.search_For_Storage(request)
      .subscribe({
        next(result) {
          self.storages = result.items;
          console.log('self.storages', self.storages);
          self.selectedStorage = (<any>self.storages)[0];
          self.createForm(self.selectedStorage?.id);
        }
      }));
  }

  createForm(defaultUserStorageId: any) {
    this.formGroup = this.formBuilder.group({
      clientId: ['', Validators.required],
      clientName: ['', Validators.required],
      clientStorageId: [defaultUserStorageId ?? '', Validators.required],
    });
  }

  save() {
    this.client.clientId = this.formGroup.controls['clientId'].value;
    this.client.clientName = this.formGroup.controls['clientName'].value;
    this.client.clientStorageId = this.formGroup.controls['clientStorageId'].value;

    this.subscription.add(this.asaPermissionService.save_Client(this.client)
      .pipe(
        catchError((err: any) => {
          return throwError(err);
        })
      )
      .subscribe(result => {
          this.router.navigate(['clients/' + result + '/edit']);
          // this.searchForClients();
        },
        err => console.log('HTTP Error', err),
        () => console.log('HTTP request completed.')
      ));
  }

  // searchForClients() {
  //   const clientRequest = new ClientPrincipalSearchRequest();
  //   clientRequest.clientId = this.client.clientId;
  //   this.asaPermissionService.search_For_Clients(clientRequest)
  //     .subscribe(result => {
  //       console.log(result);
  //       if (result.items && result.items.length > 0) {
  //         this.savedClient = result.items[0];
  //         this.router.navigate(['clients/' + this.savedClient.id + '/edit']);
  //       }
  //     });
  // }

}
