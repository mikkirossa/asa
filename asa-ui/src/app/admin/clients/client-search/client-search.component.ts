import {Component, OnDestroy, OnInit} from '@angular/core';
import {
  ClientPrincipal,
  ClientPrincipalSearchRequest,
  ASAPermissionService,
  Realm, RealmSearchRequest,
  Role, RoleSearchRequest
} from '../../../../../../asa-ui/src/app/service/admin.service';

import {ClientMenuRenderer} from "../../../shared/renderers/client-menu-renderer.component";
import {Router} from "@angular/router";
import {Constants} from "../../../utils/constants";
import {CommonService} from "../../../service/common.service";
import {UserService} from '../../../service/user.service';
import {Subscription} from "rxjs";

@Component({
  selector: 'app-client-search',
  templateUrl: './client-search.component.html',
  styleUrls: ['./client-search.component.css']
})

export class ClientSearchComponent implements OnInit, OnDestroy {
  gridDataFetching: number = 0;
  colDefKey = Constants.ComponentKeySearchClients;
  itemRowData: ClientPrincipal[] | undefined;
  realms: Realm[] | undefined;
  roles: Role[] | undefined;
  frameworkComponents: { clientMenuRenderer: any } | undefined;
  subscription: Subscription = new Subscription();

  itemColumnDefinitions = [
    {headerName: '', field: '', cellRenderer: "clientMenuRenderer", autoHeight: true, width: 125},
    {headerName: 'Name', field: 'clientName', resizable: true},
    {headerName: 'Client ID', field: 'clientId', resizable: true},
    {headerName: 'Active?', field: 'isActive', resizable: true}
  ];

  request: ClientPrincipalSearchRequest = new ClientPrincipalSearchRequest();

  constructor(private router: Router,
              private asaPermissionService: ASAPermissionService,
              private commonService: CommonService,
              public userService: UserService) {
  }

  ngOnInit() {
    this.loadRealms();
    this.clearRequest();

    this.itemRowData = [];

    this.frameworkComponents = {
      clientMenuRenderer: ClientMenuRenderer
    };
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  clearRequest() {
    this.request = new ClientPrincipalSearchRequest();
    this.request.isActiveOnly = true;
    this.roles = [];
  }

  loadRealms() {
    const self = this;
    const request = new RealmSearchRequest();
    this.subscription.add(this.asaPermissionService.search_For_Realms(request)
      .subscribe({
        next(result) {
          self.realms = result.items;
        }
      }));
  }

  loadRoles(realmId: string) {
    const self = this;
    const request = new RoleSearchRequest();
    request.realmId = realmId;

    this.subscription.add(this.asaPermissionService.search_For_Roles(request)
      .subscribe({
        next(result) {
          self.roles = result.items;
        }
      }));
  }

  selectRealm() {
    this.loadRoles(<string>this.request.realmId);
  }

  searchForClients() {

    const clientRequest = new ClientPrincipalSearchRequest();

    clientRequest.name = this.request.name;
    clientRequest.isActiveOnly = this.request.isActiveOnly;
    clientRequest.realmId = this.request.realmId;
    clientRequest.roleId = this.request.roleId;

    const self = this;
    this.gridDataFetching++;
    this.subscription.add(this.asaPermissionService.search_For_Clients(clientRequest)
      .subscribe({
        next(result) {
          self.itemRowData = result.items;
        }
      }));
  }

  openAddModal() {
    let modalData = {
      componentKey: this.colDefKey,
      actionKey: Constants.ActionKeyNew
    };
    this.commonService.setGridRowSubject(modalData);
  }
}
