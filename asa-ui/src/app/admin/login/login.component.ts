import { Component, OnInit, ElementRef, Injectable, Input } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map, retry, retryWhen } from 'rxjs/operators';
import { AuthService } from '../../service/auth.service';
import { CacheService } from '../../service/cache.service';
import { Constants } from '../../utils/constants';
import { iCons } from '../../utils/icons';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

@Injectable()
export class LoginComponent implements OnInit {
  focus: any;
  focus1: any;
  focus2: any;
  test: Date = new Date();
  private toggleButton: any;
  private sidebarVisible: boolean;
  private nativeElement: Node;
  @Input()
  userName = '';
  @Input()
  password = '';
  errorMessage: string = '';
  loginDisabled: boolean = false;
  loginStatus: string = 'Login';
  constructor(private element: ElementRef, private authService: AuthService,
    private cacheService: CacheService,
    private route: ActivatedRoute,
    private router: Router  ) {
    this.nativeElement = element.nativeElement;
    this.sidebarVisible = false;
  }
  /*
  checkFullPageBackgroundImage(){
      var $page = $('.full-page');
      var image_src = $page.data('image');

      if(image_src !== undefined){
          var image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>'
          $page.append(image_container);
      }
  };
  */

  onLogin() {
    console.log('login start...');
    this.loginDisabled = true;
    this.loginStatus = 'Loging In...';
    this.authService.login(this.userName, this.password);
    setTimeout(() => { this.cacheService.setUserSettings(); }, 2000);
  }

  ngOnInit() {
    //this.checkFullPageBackgroundImage();

    var body = document.getElementsByTagName('body')[0];
    if (body)
      body.classList.add('login-page');
    var navbar: HTMLElement = this.element.nativeElement;
    this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];

    this.authService.logout(false);

    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd))
      .subscribe((event: any) => {
        if (event.url != undefined && event.url?.indexOf(Constants.ERROR_KEY) > -1) {
          this.errorMessage = Constants.ERROR_LoginFailed;
          this.loginDisabled = false;
          this.loginStatus = 'Login';
        }
      });



    /*
    setTimeout(function(){
        // after 1000 ms we add the class animated to the login/register card
        $('.card').removeClass('card-hidden');
    }, 700)
    */
  }
  ngOnDestroy() {

    var body = document.getElementsByTagName('body')[0];
    if (body)
      body.classList.remove('login-page');

  }

  sidebarToggle() {
    /*
    var toggleButton = this.toggleButton;
    var body = document.getElementsByTagName('body')[0];
    var sidebar = document.getElementsByClassName('navbar-collapse')[0];
    if(this.sidebarVisible == false){
        setTimeout(function(){
            toggleButton.classList.add('toggled');
        },500);
        body.classList.add('nav-open');
        this.sidebarVisible = true;
    } else {
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    }
    */
  }
}
