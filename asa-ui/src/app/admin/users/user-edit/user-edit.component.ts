import { Component, OnInit } from '@angular/core';
import { TitleService } from '../../../service/title.service';
import { UserPrincipal, ASAPermissionService, RealmSearchRequest } from '../../../service/admin.service';
import { ActivatedRoute, Router } from '@angular/router';
//import Swal from 'sweetalert2';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Constants } from '../../../utils/constants';
import { CacheService } from '../../../service/cache.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  gridDataFetching: number = 0;
  formGroup: FormGroup = this.formBuilder.group({
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    email: ['', Validators.required],
    userName: ['', Validators.required],
    userStorage: ['', Validators.required],
  });
  user!: UserPrincipal;
  oldUserData!: UserPrincipal;
  colDefKey = Constants.ComponentKeyUsersRealms;
  itemRowData: UserPrincipal[] | undefined;
  addClientOrUserKey: any;

  constructor(private formBuilder: FormBuilder,
    private titleService: TitleService,
    private asaPermissionService: ASAPermissionService,
    private activatedRoute: ActivatedRoute,
    private cacheService: CacheService,
    private router: Router) {
    this.addClientOrUserKey = new Constants.addClientOrUserKey();
  }

  ngOnInit() {
    const self = this;
    self.titleService.updateTitle('User Edit');
    this.activatedRoute.params.subscribe(params => {
      self.asaPermissionService.get_User(params['principalid'])
        .subscribe((result: any) => {
          this.user = result;
          this.oldUserData = JSON.parse(JSON.stringify(this.user));
          this.createForm();
          this.loadRealms();
        });
    });
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      firstname: [this.user.firstName, Validators.required],
      lastname: [this.user.lastName, Validators.required],
      email: [this.user.emailAddress, Validators.required],
      userName: [this.user.userName, Validators.required],
      userStorage: [this.user.userStorage, Validators.required],
    });
  }

  loadRealms() {
    if (this.cacheService.allRoles.length == 0 || this.cacheService.allRoles == null || this.cacheService.allRoles == undefined) {
      setTimeout(() => { this.loadRealms(); }, 500);
    }
    else {
      const request = new RealmSearchRequest();
      this.gridDataFetching++;
      this.asaPermissionService.search_For_Realms(request)
        .subscribe((result: any) => {
          result.items?.forEach((realm: any) => {
            realm.selectedUserId = this.user.id;
            realm.addClientOrUserKey = this.addClientOrUserKey.user;
            this.user.realms?.forEach((uRealm) => {
              if (realm.id == uRealm.realmId) {
                realm.roles = uRealm.roles;
              }
            });
          });
          console.log('with roles', result.items);
          this.itemRowData = result.items;
        });
    }
  }

  resetPassword() {
    const self = this;
    //Swal.fire({
    //  title: 'Are you sure?',
    //  text: 'Resetting the password will generate a new password for the user. They will need to login with the new password going forward.',
    //  type: 'warning',
    //  showCancelButton: true,
    //  confirmButtonClass: 'btn btn-success',
    //  cancelButtonClass: 'btn btn-danger',
    //  confirmButtonText: 'Reset it!',
    //   buttonsStyling: false
    //}).then((result: any) => {
    //  if (result.value) {
    //    self.userService.reset_User_Password(self.user.id)
    //      .subscribe({
    //        next() {
    //          Swal.fire(
    //            {
    //              title: 'Password Reset!',
    //              text: 'The new password has been sent to the email addresss of the user',
    //              type: 'success',
    //              confirmButtonClass: 'btn btn-success',
    //              buttonsStyling: false
    //            });
    //        }
    //      });
    //  }
    //})
  }

  save() {
    const self = this;

    this.user.firstName = this.formGroup.controls['firstname'].value;
    this.user.lastName = this.formGroup.controls['lastname'].value;
    this.user.emailAddress = this.formGroup.controls['email'].value;

    //this.userService.update_User(this.user.id, this.user)
    //  .subscribe({
    //    next(result) {
    //      //Swal.fire('User Saved', 'Save Successful!');
    //      self.router.navigate(['//user', self.user.id, 'view']);
    //    }
    //  });
  }

  resetForm() {
    this.user = this.oldUserData;
    this.createForm();
  }
}
