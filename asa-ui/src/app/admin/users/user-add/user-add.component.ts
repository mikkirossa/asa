import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import {
  UserPrincipal, ASAPermissionService,
  PrincipalStorage, PrincipalStorageSearchRequest, AddUserRequest, RealmSearchRequest, UserPrincipalSearchRequest
} from '../../../service/admin.service';
import { TitleService } from '../../../service/title.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from '../../../utils/constants';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { CacheService } from '../../../service/cache.service';
//import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {

  formGroup: FormGroup = new FormGroup({
    firstname: new FormControl('', [Validators.required]),
    lastname: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required]),
    userstorageid: new FormControl('', [Validators.required]),
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    confirmpassword: new FormControl('', [Validators.required]),
  });

  user!: AddUserRequest;
  savedUser!: UserPrincipal;
  storages: PrincipalStorage[] | undefined;
  selectedStorage: PrincipalStorage | undefined;
  colDefKey = Constants.ComponentKeyUsersRealms;
  itemRowData: UserPrincipal[] | undefined;

  constructor(private formBuilder: FormBuilder,
    private titleService: TitleService,
    private cacheService: CacheService,
    private asaPermissionService: ASAPermissionService,
    private router: Router) { }


  

  get f() {
    return this.formGroup.controls;
  }

  ngOnInit() {
    const self = this;
    self.titleService.updateTitle('Add New User');
    this.user = new UserPrincipal();
    
    this.loadStorages();
  }

  loadStorages() {
    const self = this;
    const request = new PrincipalStorageSearchRequest();
    this.asaPermissionService.search_For_Storage(request)
      .subscribe({
        next(result) {
          self.storages = result.items;
          console.log('self.storages', self.storages);
          self.selectedStorage = (<any>self.storages)[0];
          self.createForm(self.selectedStorage?.id);
          self.changePasswordProps();
        }
      });
  }

  changePasswordProps() {
    this.storages?.forEach((s:any) => {
      if (s.id === this.f.userstorageid.value) {
        this.selectedStorage = s;

        if (this.selectedStorage?.canSetPassword) {
          console.log('validators set');
          this.formGroup.get('password')?.setValidators([Validators.required]);
          this.formGroup.get('confirmpassword')?.setValidators([Validators.required]);
        } else {
          console.log('validators cleared');
          this.formGroup.get('password')?.clearValidators();
          this.formGroup.get('confirmpassword')?.clearValidators();
        }

        this.formGroup.get('password')?.updateValueAndValidity();
        this.formGroup.get('confirmpassword')?.updateValueAndValidity();
      }
    })
  }

  createForm(defaultUserStorageId: any) {
    this.formGroup = this.formBuilder.group({
      firstname: ['', Validators.required ],
      lastname: ['', Validators.required ],
      email: ['', Validators.required ],
      userstorageid: [defaultUserStorageId ?? '', Validators.required ],
      username: ['', Validators.required ],
      password: ['', Validators.required ],
      confirmpassword: ['', Validators.required ],
    });
  }

  passwordConfirmed() {
    return this.formGroup.controls['password'].value === this.formGroup.controls['confirmpassword'].value;
  }

  save() {
    const self = this;

    this.user.firstName = this.formGroup.controls['firstname'].value;
    this.user.lastName = this.formGroup.controls['lastname'].value;
    this.user.emailAddress = this.formGroup.controls['email'].value;
    this.user.userStorageId = this.formGroup.controls['userstorageid'].value;
    this.user.userName = this.formGroup.controls['username'].value;
    this.user.password = this.formGroup.controls['password'].value;

    this.asaPermissionService.add_User(this.user)
      .pipe(
        catchError((err: any) => {
          return throwError(err);
        })
      )
      .subscribe(result => {
        console.log(result);
        if (result != undefined && result != null) {
          this.router.navigate(['users/' + result + '/edit']);
        }
        else {
          this.searchForUsers();
        }
        //Swal.fire('User Added', 'Save Successful!');
          //self.router.navigate(['/user', 'search']);
        },
        err => console.log('HTTP Error', err),
        () => console.log('HTTP request completed.')
      );
  }

  searchForUsers() {
    const userRequest = new UserPrincipalSearchRequest();
    userRequest.userName = this.user.userName;
    this.asaPermissionService.search_For_Users(userRequest)
      .subscribe(result => {
        console.log(result);
        if (result.items && result.items.length > 0) {
          this.savedUser = result.items[0];
          //this.loadRealms();
          this.router.navigate(['users/' + this.savedUser.id + '/edit']);
        }
      });
  }

  loadRealms() {
    if (this.cacheService.allRoles.length == 0 || this.cacheService.allRoles == null || this.cacheService.allRoles == undefined) {
      setTimeout(() => { this.loadRealms(); }, 500);
    }
    else {
      const request = new RealmSearchRequest();
      this.asaPermissionService.search_For_Realms(request)
        .subscribe((result: any) => {
          result.items?.forEach((realm: any) => {
            realm.selectedUserId = this.savedUser.id;
            (<any>this.savedUser).realms?.forEach((uRealm: any) => {
              if (realm.id == uRealm.realmId) {
                realm.roles = uRealm.roles;
              }
            });
          });
          console.log('with roles', result.items);
          this.itemRowData = result.items;
        });
    }
  }
}
