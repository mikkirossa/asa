import { Component, OnInit } from '@angular/core';
import { UserPrincipal, UserPrincipalSearchRequest, PrincipalStorage, Realm, PrincipalStorageSearchRequest,
  RealmSearchRequest, Role, RoleSearchRequest, ASAPermissionService
} from '../../../service/admin.service';
import { TitleService } from '../../../service/title.service';
import { Router } from '@angular/router';
import { UserMenuRenderer } from '../../../shared/renderers/user-menu-renderer.component';
import { Constants } from '../../../utils/constants';
import { CommonService } from '../../../service/common.service';
import { UserService } from '../../../service/user.service';

@Component({
  selector: 'app-user-search',
  templateUrl: './user-search.component.html',
  styleUrls: ['./user-search.component.css']
})
export class UserSearchComponent  implements OnInit {

  gridDataFetching: number = 0;
  colDefKey = Constants.ComponentKeySearchUsers;
  itemRowData: UserPrincipal[] | undefined;
  storages: PrincipalStorage[] | undefined;
  realms: Realm[] | undefined;
  roles: Role[] | undefined;
  frameworkComponents: { userMenuRenderer: any; } | undefined;

  request: UserPrincipalSearchRequest = new UserPrincipalSearchRequest();

  constructor(private titleService: TitleService,
    private router: Router,
    private asaPermissionService: ASAPermissionService,
    private commonService: CommonService,
    public userService: UserService) { }

  ngOnInit() {
    const self = this;
    self.titleService.updateTitle('User Search');
    this.loadStorages();
    this.loadRealms();
    this.clearRequest();

    this.itemRowData = [];

    this.frameworkComponents = {
      userMenuRenderer: UserMenuRenderer
    };
  }

  assignRequestProps() {
    this.request.userStorageId = "";
    this.request.realmId = "";
    this.request.roleId = "";
  }

  clearRequest() {
    this.request = new UserPrincipalSearchRequest();
    this.request.isActiveOnly = true;
    this.roles = [];
    this.assignRequestProps();
  }

  loadStorages() {
    const self = this;
    const request = new PrincipalStorageSearchRequest();
    this.asaPermissionService.search_For_Storage(request)
      .subscribe({
        next(result) {
          self.storages = result.items;
        }
      });
  }

  loadRealms() {
    const self = this;
    const request = new RealmSearchRequest();
    this.asaPermissionService.search_For_Realms(request)
      .subscribe({
        next(result) {
          self.realms = result.items;
        }
      });
  }

  loadRoles(realmId: string) {
    const self = this;
    const request = new RoleSearchRequest();
    request.realmId = realmId;

    this.asaPermissionService.search_For_Roles(request)
      .subscribe({
        next(result) {
          self.roles = result.items;
        }
      });
  }

  selectStorage() {
  }

  selectRealm() {
    this.loadRoles(<string>this.request.realmId);
  }

  searchForUsers() {

    const userRequest = new UserPrincipalSearchRequest();

    userRequest.userName = this.request.userName;
    userRequest.emailAddress = this.request.emailAddress;
    userRequest.firstName = this.request.firstName;
    userRequest.lastName = this.request.lastName;
    userRequest.isActiveOnly = this.request.isActiveOnly;
    userRequest.userStorageId = this.request.userStorageId;
    userRequest.realmId = this.request.realmId;
    userRequest.roleId = this.request.roleId;

    const self = this;
    this.gridDataFetching++;
    this.asaPermissionService.search_For_Users(userRequest)
      .subscribe({
        next(result) {
          self.itemRowData = result.items;
        }
      });
  }

  openAddModal() {
    let modalData = {
      componentKey: this.colDefKey,
      actionKey: Constants.ActionKeyNew
    };
    console.log(modalData);
    this.commonService.setGridRowSubject(modalData);
  }

  resetForm() {
    this.clearRequest();
  }
}
