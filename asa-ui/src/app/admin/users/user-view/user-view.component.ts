import { Component, OnInit } from '@angular/core';
import { TitleService } from '../../../service/title.service';
import { UserPrincipal, PrincipalRealm, Role, Permission,
  ASAPermissionService, RoleSearchRequest, PermissionSearchRequest } from '../../../service/admin.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})
export class UserViewComponent implements OnInit {

  user!: UserPrincipal;

  realms: Array<PrincipalRealm> = new Array<PrincipalRealm>();
  roleRowData!: Role[];
  permissionRowData!: Permission[];
  selectedRealmId!: string;

  roleColumnDefinitions = [
    {headerName: 'Name', field: 'name', resizable: true },
    {headerName: 'Key', field: 'key', resizable: true },
    {headerName: 'Description', field: 'description', resizable: true }
  ];

  permissionColumnDefinitions = [
    {headerName: 'Name', field: 'name', resizable: true },
    {headerName: 'Key', field: 'key', resizable: true },
    {headerName: 'Description', field: 'description', resizable: true }
  ];

  constructor(private titleService: TitleService,
    private asaPermissionService: ASAPermissionService,
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    const self = this;
    self.titleService.updateTitle('User View');
    this.roleRowData = [];
    this.permissionRowData = [];
    this.activatedRoute.params.subscribe(params => {
      self.asaPermissionService.get_User(params['principalid'])
        .subscribe({
          next(results: any) {
            self.user = results;
            self.realms = <any>self.user.realms;
          }
        });
    });
  }

  selectPermissions() {
    const self = this;
    let permissionIds: string[];
    permissionIds = [];

    this.roleRowData.forEach(function(role: any) {
      role.permissionIds.forEach(function(permissionId: any) {
        permissionIds.push(permissionId);
      });
    });

    if (permissionIds.length == 0) {
      self.permissionRowData = [];
    } else {
      const request = new PermissionSearchRequest();
      request.ids = permissionIds;

      this.asaPermissionService.search_For_Permissions(request)
        .subscribe({
          next(result: any) {
            self.permissionRowData = result.items;
          }
        });
    }
  }

  selectRealm() {
    let roleIds: string[];
    roleIds = [];
    const self = this;

    this.realms.forEach(function(realm: any) {
      if (realm.realmId === self.selectedRealmId) {
        realm.roles.forEach(function(role: any) {
          roleIds.push(role.roleId);
        });
      }
    });

    if (roleIds.length == 0) {
      self.roleRowData = [];
    } else {
      const request = new RoleSearchRequest();
      request.ids = roleIds;

      this.asaPermissionService.search_For_Roles(request)
        .subscribe({
          next(result: any) {
            self.roleRowData = result.items;
            self.selectPermissions();
          }
        });
    }
  }
}

