import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ASAPermissionService, ClientPrincipal, ClientPrincipalSearchRequest, Permission, PermissionSearchRequest, PrincipalStorage, PrincipalStorageSearchRequest, Realm, Role, RoleSearchRequest, UserPrincipal, UserPrincipalSearchRequest } from '../../../service/admin.service';
import { CacheService } from '../../../service/cache.service';
import { CommonService } from '../../../service/common.service';
import { TitleService } from '../../../service/title.service';
import { Constants } from '../../../utils/constants';
import { MasterComponent } from '../../master/master.component';

@Component({
  selector: 'app-realm-edit',
  templateUrl: './realm-edit.component.html',
  styleUrls: ['./realm-edit.component.css']
})
export class RealmEditComponent extends MasterComponent implements OnInit {
  formGroup: FormGroup = this.formBuilder.group({
    key: ['', Validators.required],
    name: ['', Validators.required],
    userStorageId: ['', Validators.required],
    clientStorageId: ['', Validators.required],
    description: ['', Validators.required],
    tokenLifetimeInMinutes: ['', Validators.required],
  });

  realm!: Realm;
  oldRealmData!: Realm;
  storages: PrincipalStorage[] | undefined;
  colDefKeyUsersEditRealm = Constants.ComponentKeySearchUsersEditRealm;
  itemRowDataUsers: UserPrincipal[] | undefined;
  colDefKeyClientsEditRealm = Constants.ComponentKeySearchClientsEditRealm;
  itemRowDataClients: ClientPrincipal[] | undefined;

  colDefKeyRolesEditRealm = Constants.ComponentKeySearchRolesEditRealm;
  itemRowDataRoles: Role[] | undefined;
  colDefKeyPermissionsEditRealm = Constants.ComponentKeySearchPermissionsEditRealm;
  itemRowDataPermissions: Permission[] | undefined;
  subscription: Subscription | undefined;
  isReadOnly: boolean = true;


  constructor(private formBuilder: FormBuilder,
    private titleService: TitleService,
    private asaPermissionService: ASAPermissionService,
    private activatedRoute: ActivatedRoute,
    private cacheService: CacheService,
    private router: Router,
    public commonService: CommonService  ) {
    super(commonService);
    this.loadStorages();
    this.subscription = this.commonService.getEditRealmFlagSubject().subscribe(x => {
      console.log('Edit realm', x);
      if (x != undefined && x != null && x != '')
        this.isReadOnly = false;
      else
        this.isReadOnly = true;
    });

    this.subscription = this.commonService.getEditRealmGridRefreshSubject().subscribe(x => {
      console.log('Refresh grids', x);
      this.refreshGrids(x);
    });
  }

  ngOnInit() {
    const self = this;
    this.getRealmIdFromUrl();
    self.titleService.updateTitle('User Edit');
    this.activatedRoute.params.subscribe(params => {
      self.asaPermissionService.get_Realm(params['realmId'])
        .subscribe((result: any) => {
          this.realm = result;
          this.oldRealmData = JSON.parse(JSON.stringify(this.realm));
          this.createForm();
          this.searchForUsers();
          this.searchForClients();
          this.searchForRoles();
          this.searchForPermissions();
        });
    });
  }

  refreshGrids(name: string) {
    switch (name) {
      case Constants.ID_User:
        this.searchForUsers();
        break;
      case Constants.ID_Client:
        this.searchForClients();
        break;
      case Constants.ID_Role:
        this.searchForRoles();
        break;
      case Constants.ID_Permission:
        this.searchForPermissions();
        break;
    }
  }

  getRealmIdFromUrl() {
    this.activatedRoute.params.subscribe( params => {
      const realm = params.realmId;
      console.log('This is realm params', realm)
    })
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      key: [this.realm.key, Validators.required],
      name: [this.realm.name, Validators.required],
      userStorageId: [this.realm.userStorageId, Validators.required],
      clientStorageId: [this.realm.clientStorageId, Validators.required],
      description: [this.realm.description, Validators.required],
      tokenLifetimeInMinutes: [this.realm.tokenLifetimeInMinutes, Validators.required],
    });
  }

  loadStorages() {
    const self = this;
    const request = new PrincipalStorageSearchRequest();
    this.asaPermissionService.search_For_Storage(request)
      .subscribe({
        next(result) {
          self.storages = result.items;
        }
      });
  }

  searchForUsers() {
    const userRequest = new UserPrincipalSearchRequest();
    userRequest.realmId = this.realm.id;
    const self = this;
    this.asaPermissionService.search_For_Users(userRequest)
      .subscribe({
        next(result) {
          self.itemRowDataUsers = result.items;
          self.itemRowDataUsers = (<any>self.itemRowDataUsers).map((obj: any) => ({ ...obj, realmId: self.realm.id }));
        }
      });
  }

  searchForClients() {
    const clientRequest = new ClientPrincipalSearchRequest();
    clientRequest.realmId = this.realm.id;
    const self = this;
    this.asaPermissionService.search_For_Clients(clientRequest)
      .subscribe({
        next(result) {
          self.itemRowDataClients = result.items;
          self.itemRowDataClients = (<any>self.itemRowDataClients).map((obj: any) => ({ ...obj, realmId: self.realm.id }));
        }
      });
  }

  searchForRoles() {
    const roleRequest = new RoleSearchRequest();
    roleRequest.realmId = this.realm.id;
    const self = this;
    this.asaPermissionService.search_For_Roles(roleRequest)
      .subscribe({
        next(result) {
          self.itemRowDataRoles = result.items;
          self.itemRowDataRoles = (<any>self.itemRowDataRoles).map((obj: any) => ({ ...obj, realmId: self.realm.id }));
        }
      })
  }

  searchForPermissions() {
    const permissionRequest = new PermissionSearchRequest();
    permissionRequest.realmId = this.realm.id;
    const self = this;
    this.asaPermissionService.search_For_Permissions(permissionRequest)
      .subscribe({
        next(result) {
          self.itemRowDataPermissions = result.items;
          self.itemRowDataPermissions = (<any>self.itemRowDataPermissions).map((obj: any) => ({ ...obj, realmId: self.realm.id }));
        }
      })
  }

  save() {
    const self = this;
    //this.user.firstName = this.formGroup.controls['firstname'].value;
    //this.user.lastName = this.formGroup.controls['lastname'].value;
    //this.user.emailAddress = this.formGroup.controls['email'].value;
    //this.user.userStorageId = this.formGroup.controls['userstorageid'].value;
    //this.user.userName = this.formGroup.controls['username'].value;
    //this.user.password = this.formGroup.controls['password'].value;

    //this.asaPermissionService.update_Realm(this.user)
    //  .pipe(
    //    catchError((err: any) => {
    //      return throwError(err);
    //    })
    //  )
    //  .subscribe(result => {
    //    console.log(result);
    //    if (result != undefined && result != null) {
    //      this.router.navigate(['users/' + result + '/edit']);
    //    }
    //    else {
    //      this.searchForUsers();
    //    }
    //    //Swal.fire('User Added', 'Save Successful!');
    //    //self.router.navigate(['/user', 'search']);
    //  },
    //    err => console.log('HTTP Error', err),
    //    () => console.log('HTTP request completed.')
    //  );
  }

  resetForm() {
    this.realm = this.oldRealmData;
    this.createForm();
  }

  addClientToRealm() {
    let modalData = {
      componentKey: Constants.ComponentKeyAddClientsToRealm,
      actionKey: Constants.ActionKeyNew,
      data: this.realm
    };
    this.commonService.setGridRowSubject(modalData);
  }

  addUserToRealm() {
    let modalData = {
      componentKey: Constants.ComponentKeyAddUsersToRealm,
      actionKey: Constants.ActionKeyNew,
      data: this.realm
    };
    this.commonService.setGridRowSubject(modalData);
  }

  addRolesToRealm() {
    let modalData = {
      componentKey: Constants.ComponentKeyAddRolesToRealm,
      actionKey: Constants.ActionKeyNew,
      data: this.realm
    };
    this.commonService.setGridRowSubject(modalData);
  }

  addPermissionsToRealm() {
    let modalData = {
      componentKey: Constants.ComponentKeyAddPermissionsToRealm,
      actionKey: Constants.ActionKeyNew,
      data: this.realm
    };
    this.commonService.setGridRowSubject(modalData);
  }
}
