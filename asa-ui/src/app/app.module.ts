import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './shared/header/header.component';
import {FooterComponent} from './shared/footer/footer.component';
import {SidebarComponent} from './shared/sidebar/sidebar.component';
import {HomeComponent} from './admin/home/home.component';
import {LoginComponent} from './admin/login/login.component';
import {AuthService} from './service/auth.service';
import {ASAPermissionService} from './service/admin.service';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AgGridModule} from 'ag-grid-angular';
import {GridComponent} from './grid/grid/grid.component';
import {ActionButtonsComponent} from './grid/action-buttons/action-buttons.component';
import {BaseModalComponent} from './modalPopups/base-modal/base-modal.component';
import {DeleteComponent} from './modalPopups/delete/delete.component';
import {SuccessComponent} from './modalPopups/success/success.component';
import {JwtInterceptor} from './service/jwt.interceptor';
import {TitleService} from './service/title.service';
import {MasterComponent} from './admin/master/master.component';
import {AlphaNumPipe} from './pipes/alpha-num.pipe';
import {CacheService} from './service/cache.service';
import {FullUserNamePipe} from './pipes/full-user-name.pipe';
import {AuthGuard} from './service/auth-guard.service';
import {HasPermissionDirective} from './directives/has-permission.directive';
import {NumbersOnlyDirective} from './directives/numbers-only.directive';
import {ErrorIntercept} from './service/error.interceptor';
import {MyLoaderComponent} from './shared/my-loader/my-loader.component';
import {MarkAsteriskDirective} from './directives/mark-asterisk.directive';
import {UserAddComponent} from './admin/users/user-add/user-add.component';
import {UserMenuRenderer} from './shared/renderers/user-menu-renderer.component';
import {UserEditComponent} from './admin/users/user-edit/user-edit.component';
import {UserSearchComponent} from './admin/users/user-search/user-search.component';
import {UserViewComponent} from './admin/users/user-view/user-view.component';
import {ClientMenuRenderer} from './shared/renderers/client-menu-renderer.component';
import {PermissionMenuRenderer} from './shared/renderers/permission-menu-renderer.component';
import {RoleMenuRenderer} from './shared/renderers/role-menu-renderer.component';
import {CommonService} from './service/common.service';
import {ClientSearchComponent} from './admin/clients/client-search/client-search.component';
import {ClientAddComponent} from "./admin/clients/client-add/client-add.component";
import {CommonModule} from "@angular/common";
import {DropdownComponent} from './grid/dropdown/dropdown.component';
import {SelectedRolesComponent} from './grid/selected-roles/selected-roles.component';
import {ViewUserPermissionsComponent} from './grid/view-permissions/view-user-permissions.component';
import {ViewPermissionsComponent} from './modalPopups/view-permissions/view-permissions.component';
import {AlertClientComponent} from "./modalPopups/client/alert-client/alert-client.component";
import {ClientEditComponent} from "./admin/clients/client-edit/client-edit.component";
import {AlertUserComponent} from './modalPopups/user/alert-user/alert-user.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { MatSelectModule } from '@angular/material/select';
import { RealmEditComponent } from './admin/realm/realm-edit/realm-edit.component';
import { SettingsComponent } from './admin/settings/settings.component';
import {AddRealmComponent} from "./modalPopups/realm/add-realm/add-realm-component";
import {AddClientsToRealmComponent} from "./modalPopups/realm/add-clients-to-realm/add-clients-to-realm.component";
import { FilterPipe } from './pipes/filter-by.pipe';
import { CheckboxComponent } from './grid/checkbox/checkbox.component';
import { AddUsersToRealmComponent } from './modalPopups/realm/add-users-to-realm/add-users-to-realm.component';
import {AddRolesToRealmComponent} from "./modalPopups/realm/add-roles-to-realm/add-roles-to-realm.component";
import {AddPermissionsToRealmComponent} from "./modalPopups/realm/add-permissions-to-realm/add-permissions-to-realm.component";
import {StatusComponent} from "./grid/status/status.component";
import { EditRolesToRealmComponent } from './modalPopups/realm/edit-roles-to-realm/edit-roles-to-realm.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    HomeComponent,
    LoginComponent,
    GridComponent,
    ActionButtonsComponent,
    BaseModalComponent,
    DeleteComponent,
    SuccessComponent,
    MasterComponent,
    AlphaNumPipe,
    FullUserNamePipe,
    FilterPipe,
    HasPermissionDirective,
    NumbersOnlyDirective,
    MyLoaderComponent,
    MarkAsteriskDirective,
    UserAddComponent,
    UserMenuRenderer,
    UserEditComponent,
    UserSearchComponent,
    UserViewComponent,
    ClientMenuRenderer,
    PermissionMenuRenderer,
    RoleMenuRenderer,
    ClientSearchComponent,
    ClientAddComponent,
    ClientEditComponent,
    AlertClientComponent,
    RoleMenuRenderer,
    DropdownComponent,
    SelectedRolesComponent,
    ViewPermissionsComponent,
    ViewUserPermissionsComponent,
    StatusComponent,
    AlertUserComponent,
    RealmEditComponent,
    SettingsComponent,
    AddRealmComponent,
    AddRealmComponent,
    CheckboxComponent,
    AddUsersToRealmComponent,
    AddClientsToRealmComponent,
    AddRolesToRealmComponent,
    AddPermissionsToRealmComponent,
    EditRolesToRealmComponent
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    AgGridModule.withComponents(null),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatSlideToggleModule,
    MatSelectModule,
    ReactiveFormsModule,
  ],
  providers: [
    ASAPermissionService,
    AuthService,
    CacheService,
    TitleService,
    HttpClientModule,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorIntercept,
      multi: true
    },
    AuthGuard,
    CommonService,
  ],
  bootstrap: [AppComponent],
  exports: [HasPermissionDirective]
})
export class AppModule {
}
