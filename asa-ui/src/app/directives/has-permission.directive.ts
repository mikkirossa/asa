import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { CacheService } from '../service/cache.service';
import { UserService } from '../service/user.service';
import { Config } from '../utils/config';

@Directive({
  selector: '[hasPermission]'
})
export class HasPermissionDirective {
  private currentUser: string | undefined;
  private permissionsRequired: Array<string> = [];
  private usersPermissions: Array<string> = [];
  private op: string | undefined;

  constructor(
    private element: ElementRef,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private userService: UserService,
    private authService: AuthService,
    private cacheService: CacheService
  ) {
  }

  ngOnInit() {
    this.usersPermissions = this.userService.getUserPermissions();
    this.currentUser = this.cacheService.getUserNameFromSettings();
    this.updateView();
  }

  @Input()
  set hasPermission(val: any) {
    this.permissionsRequired = val;
    this.permissionsRequired.push(Config.Permissions.CONFIG_ADMIN);

    // replace RWD, RW0, 0WD, R0D with R, W, D
    this.permissionsRequired.forEach((p: string) => {
      if (p != undefined && p.length > 0) {
        let lastIndex = p.lastIndexOf('_');
        let currPermission = p.substr(0, lastIndex);
        let currAccess = p.substr(lastIndex + 1);

        if (currAccess.length > 1 && (currAccess.indexOf('RWD') > -1 || currAccess.indexOf('RW0') > -1 || currAccess.indexOf('0WD') > -1 || currAccess.indexOf('R0D') > -1)) {
          [...currAccess].forEach(c => {
            this.permissionsRequired.push(currPermission + '_' + c);
          });
        }
      }
    });
    this.updateView();
  }

  @Input()
  set hasPermissionOp(op: any) {
    this.op = op;
    this.updateView();
  }

  private updateView() {
    if (this.checkPermission()) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

  private checkPermission() {
    let hasPermission = false;

    if (this.currentUser && this.usersPermissions) {
      for (const checkPermission of this.permissionsRequired) {
        if (checkPermission != undefined && checkPermission.length > 0) {
          const permissionFound = this.usersPermissions.find((x: any) => x.toUpperCase() === (<string>checkPermission).toUpperCase());
          if (permissionFound) {
            hasPermission = true;
            if (this.op === 'OR') {
              break;
            }
          }
          else {
            hasPermission = false;
            if (this.op === 'AND') {
              break;
            }
          }
        }
      }
    }
    return hasPermission;
  }
}
