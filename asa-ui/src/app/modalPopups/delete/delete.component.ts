import { TitleCasePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { AlphaNumPipe } from '../../pipes/alpha-num.pipe';
import { CacheService } from '../../service/cache.service';
import { CommonService } from '../../service/common.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-delete-row',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {
  @Input() childModalData: any;
  message: string = "";
  deleteSuccess: boolean = false;
  constructor(private commonService: CommonService,
    private cacheService: CacheService) {
    console.log('child modal', this.childModalData);
  }

  ngOnInit(): void {
  }

  delete() {
    let deleteArgs: Array<any> = [];
    if (this.childModalData.primaryKey.indexOf('-') > -1) {
      let _pkfs = this.childModalData.primaryKey.split('-');
      _pkfs?.forEach((p: string) => {
        deleteArgs.push(this.childModalData.data[p]);
      });
    }
    else {
      deleteArgs.push(this.childModalData.data[this.childModalData.primaryKey]);
    }

    this.childModalData.service[this.childModalData.deleteFunction](...deleteArgs)
      .subscribe((op: { items: any; }) => {
        this.deleteSuccess = true;
        this.message = Constants.deleteSuccessMessage
          .replace(Constants.COMPONENTKEY, TitleCasePipe.prototype.transform(AlphaNumPipe.prototype.transform(this.childModalData.componentKey)))
          .replace(Constants.PRIMARYKEY, this.childModalData.secondaryKeyHeader)
          .replace(Constants.PRIMARYVALUE, this.childModalData.data[this.childModalData.secondaryKey]);

        this.childModalData.actionKey = Constants.ActionKeyDeleted;
        this.commonService.setDeletedGridRowSubject(this.childModalData);
        this.cacheService.reloadObject(this.childModalData.componentKey);
      });
  }

  deleteMessage() {
    return Constants.deleteMessage
      .replace(Constants.COMPONENTKEY, TitleCasePipe.prototype.transform(AlphaNumPipe.prototype.transform(this.childModalData.componentKey)))
      .replace(Constants.PRIMARYKEY, this.childModalData.secondaryKeyHeader)
      .replace(Constants.PRIMARYVALUE, this.childModalData.data[this.childModalData.secondaryKey]);
  }

  cancel() {
    this.deleteSuccess = false;
  }
}
