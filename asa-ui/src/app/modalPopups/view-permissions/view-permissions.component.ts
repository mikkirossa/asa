import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { ASAPermissionService } from '../../service/admin.service';
import { CacheService } from '../../service/cache.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-view-permissions',
  templateUrl: './view-permissions.component.html',
  styleUrls: ['./view-permissions.component.css']
})
export class ViewPermissionsComponent implements OnInit {
  @Input() childModalData: any;
  roles: Array<any> = new Array<any>();
  filteredRoles: Array<any> = new Array<any>();
  filterKey: string = '';
  hideDesc: boolean = false;
  addClientOrUserKey: any;
  openAccordion: any = '';

  constructor(
    private asaPermissionService: ASAPermissionService,
    private cacheService: CacheService,
  ) {
    this.addClientOrUserKey = new Constants.addClientOrUserKey();
  }

  ngOnInit(): void {
    this.bindData();
  }

  ngOnChanges() {
    this.filterKey = '';
    this.roles = new Array<any>();
    this.filteredRoles = new Array<any>();
    console.log('new row to edit', this.childModalData);
    this.bindData();
  }

  bindData() {
    if (this.childModalData.data.addClientOrUserKey == this.addClientOrUserKey.user) {
      this.asaPermissionService.get_User(this.childModalData.data.selectedUserId)
        .subscribe((result: any) => {
          this.processRolesAndPermissions(result);
        });
    }
    else if (this.childModalData.data.addClientOrUserKey == this.addClientOrUserKey.client) {
      this.asaPermissionService.get_Client(this.childModalData.data.selectedClientId)
        .subscribe((result: any) => {
          this.processRolesAndPermissions(result);
        });
    }
  }

  processRolesAndPermissions(result: any) {
    this.roles = new Array<any>();
    let _realmsFound = result.realms?.filter((r: any) => r.realmId == this.childModalData.data.id);
    if (_realmsFound != null && _realmsFound != undefined && _realmsFound.length > 0) {

      _realmsFound.forEach((rm: any) => {
        rm?.roles?.forEach((r: any) => {

          let _roleRealmFound = this.cacheService.allRoles?.filter((rrm: any) => rrm.realmId == rm.realmId)
          if (_roleRealmFound != null && _roleRealmFound != undefined && _roleRealmFound.length > 0) {
            let _realmRoleFound = _roleRealmFound[0].roles?.filter((rr: any) => rr.id == r.roleId)
            if (_realmRoleFound != null && _realmRoleFound != undefined && _realmRoleFound.length > 0) {
              this.roles.push({ ...r, 'permissions': _realmRoleFound[0].permissionIds });
            }
          }
        });
      });
    }
    this.roles = [...new Set(this.roles)];
    this.filteredRoles = Object.assign(this.filteredRoles, this.roles);

    // console.log('users roles with permissions', this.roles);
  }

  filterPermissions(event: any) {
    const self = this;
    this.filterKey = event.target.value;
    this.filteredRoles = JSON.parse(JSON.stringify(this.roles));

    this.filteredRoles?.forEach(function (r: any, _ind: number) {

      let filterdPermissions = r.permissions.filter((x:any) => {
        return (
        x.key.toLowerCase().includes(self.filterKey.toLowerCase())
          || x.name.toLowerCase().includes(self.filterKey.toLowerCase())
          || x.description.toLowerCase().includes(self.filterKey.toLowerCase())
          || x.realmId.toLowerCase().includes(self.filterKey.toLowerCase())
          )
      });

      self.filteredRoles[_ind].permissions = filterdPermissions;

    });

    console.log(this.filterKey, this.filteredRoles);
  }

  HideDesc() {
    this.hideDesc = !this.hideDesc;
  }

  setOpenAccordion(event: any) {
    this.openAccordion = event.target.attributes['data-bs-target'].value;
    this.openAccordion = this.openAccordion.replace('#flush-collapse-', '');
  }
}
