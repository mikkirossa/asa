import {Component, ElementRef, Input, ViewChild} from "@angular/core";
import { UserService } from "../../../service/user.service";
import { Constants } from "../../../utils/constants";

@Component({
  selector: 'app-alert-client',
  templateUrl: './alert-client.component.html',
  styleUrls: ['alert-client.component.css']
})

export class AlertClientComponent {
  @Input() childModalData: any;
  @ViewChild('closeAddEditModal')
    closeAddEditModal!: ElementRef;
  doNotShowMessageFlag: boolean = false;
  constructor(private userService: UserService) { }

  addClient() {
    this.closeAddEditModal.nativeElement.click();
  }

  saveSetting() {
    let prop_val = Constants.User_SettingProps[0].split('-')[0] + "-" + this.doNotShowMessageFlag;
    this.userService.toggleSettingProps([prop_val]);
  }

}

