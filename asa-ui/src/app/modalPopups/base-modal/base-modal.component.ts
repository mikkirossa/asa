import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { CacheService } from '../../service/cache.service';
import { CommonService } from '../../service/common.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-base-modal',
  templateUrl: './base-modal.component.html',
  styleUrls: ['./base-modal.component.css']
})
export class BaseModalComponent implements OnInit {
  subscription: Subscription | undefined;
  modalData: any;
  const: any = Constants;
  visibleMessage: boolean = false;
  items: any;
  @ViewChild('btnClosePopup')
    btnClosePopup!: ElementRef;

  constructor(private commonService: CommonService,
    public cacheService: CacheService,
    private elementRef: ElementRef  ) {
    this.subscription = this.commonService.getGridRowSubject().subscribe(x => {
      if (x.data != null && (!!x.data.updatedBy == false || x.data.updatedBy == undefined))
        if (x.data.hasOwnProperty('updatedBy'))
          x.data.updatedBy = '';
      if (x.data != null && (!!x.data.updateDate == false || x.data.updateDate == undefined))
        if (x.actionKey != Constants.ActionKeyAdded && x.data.hasOwnProperty('updateDate')) x.data.updateDate = new Date();
      this.modalData = x;
      // console.log('base modal -> subject', x);

      //check for popup condition
      if ((x.actionKey == Constants.ActionKeyAdded || x.actionKey == Constants.ActionKeyUpdated) && this.cacheService.getUserSettings().doNotShowSavePopUp == 'true') {
        this.elementRef.nativeElement.querySelector('#btnClosePopup').click();
      }

    });

    this.subscription = this.commonService.getErrorInfoSuccessMessageSubject().subscribe(x => {
      console.log('existing messages', x);
      this.showError(x);
    });
  }

  ngOnInit(): void {
  }

  showError(x: any) {
    if (x != null && x.items != null && x.items.length > 0) {
      this.visibleMessage = true;
      this.items = x.items;
    }
  }

  closeError() {
    this.visibleMessage = false;
    this.commonService.clearErrorInfoSuccessMessageSubject();
  }

}
