import { TitleCasePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlphaNumPipe } from '../../pipes/alpha-num.pipe';
import { AuthService } from '../../service/auth.service';
import { CacheService } from '../../service/cache.service';
import { CommonService } from '../../service/common.service';
import { UserService } from '../../service/user.service';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {
  @Input() childModalData: any;
  subscription: Subscription | undefined;
  data: any;
  messageTitle: string = '';
  message: string = '';
  doNotShowMessageFlag: boolean = false;
  constructor(private authService: AuthService,
    private commonService: CommonService,
    private userService: UserService,
    private cacheService: CacheService) {
    this.subscription = this.commonService.getNewGridRowSubject().subscribe(x => {
      this.data = x;
      console.log('getting success data-', x);
      this.showMessageTitle();
      this.showMessage();
    });
  }

  ngOnInit(): void {
  }

  showMessageTitle() {
    console.log('show message title');
    this.messageTitle = "Successfully " + this.data.actionKey;
  }

  showMessage() {
    if (this.data.actionKey == Constants.ActionKeyAdded)
      this.message = "You have successfully added new " + this.transformKey(this.data.componentKey) + ". Each time you add a new " + this.transformKey(this.data.componentKey) + " it will appear at the top of the list.";
    else if (this.data.actionKey == Constants.ActionKeyUpdated)
      this.message = "You have successfully updated " + this.transformKey(this.data.componentKey) + ".";
  }

  transformKey(key: string) {
    return TitleCasePipe.prototype.transform(AlphaNumPipe.prototype.transform(key))
  }

  doNotShowMessage() {
    //this.userService.editUserSetting(<string>this.authService.loginName, Constants.PROP_DoNotShowSavePopUp, this.doNotShowMessageFlag.toString())
    //  .subscribe(op => {
    //    console.log('user setting updated.');
    //    this.cacheService.setUserSettings();
    //  });
  }
}
