import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../../service/user.service';
import { Constants } from '../../../utils/constants';

@Component({
  selector: 'app-alert-user',
  templateUrl: './alert-user.component.html',
  styleUrls: ['./alert-user.component.css']
})
export class AlertUserComponent implements OnInit {
  @Input() childModalData: any;
  @ViewChild('closeAddEditModal')
  closeAddEditModal!: ElementRef;
  doNotShowMessageFlag: boolean = false;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  addUser() {
    this.closeAddEditModal.nativeElement.click();
  }

  saveSetting() {
    let prop_val = Constants.User_SettingProps[0].split('-')[0] + "-" + this.doNotShowMessageFlag;
    this.userService.toggleSettingProps([prop_val]);
  }
}
