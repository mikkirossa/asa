import {Component, ElementRef, Input, OnInit, ViewChild} from "@angular/core";
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {ASAPermissionService, Realm, Role} from "../../../service/admin.service";
import {ActivatedRoute} from "@angular/router";
import {ErrorService} from "../../../service/error.service";
import {CommonService} from "../../../service/common.service";
import {Constants} from "../../../utils/constants";

@Component({
  selector: 'app-add-roles-to-realm',
  templateUrl: './add-roles-to-realm.component.html'
})

export class AddRolesToRealmComponent implements OnInit{

  @Input() childModalData: any;
  @ViewChild('closeAddEditModal') closeAddEditModal!: ElementRef;
  role: Role = new Role();
  realm!: Realm;

  formGroup: FormGroup = new FormGroup({
    key: new FormControl(['', Validators.required]),
    name: new FormControl(['', Validators.required]),
    description: new FormControl(['', Validators.required]),
  });

  constructor(private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private asaPermissionService: ASAPermissionService,
              private errorInfoService: ErrorService,
              private commonService: CommonService) {
  }

  ngOnInit() {
    this.createForm();
    console.log('This is realm params', this.childModalData.data.id);
  }

  getRealmIdFromUrl() {
    this.activatedRoute.params.subscribe( params => {
      const realm = params;
      console.log('This is realm params', realm)
    })
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      key: [this.role.key, Validators.required],
      name: [this.role.name, Validators.required],
      description: [this.role.description, Validators.required],
    });
  }

  save() {
    const self = this;
    this.role.key = this.formGroup.controls['key'].value;
    this.role.name = this.formGroup.controls['name'].value;
    this.role.description = this.formGroup.controls['description'].value;
    this.role.realmId = this.childModalData.data.id;

    this.asaPermissionService.add_Role(this.role)
      .subscribe({
        next(result) {
          self.errorInfoService.createErrorInfoSuccessObject([{ message: Constants.addRoleStatusMessage}]);
          // self.commonService.setEditRealmGridRefreshSubject(Constants.ID_User);
          self.closeAddEditModal.nativeElement.click();
        }
      });
  }

}
