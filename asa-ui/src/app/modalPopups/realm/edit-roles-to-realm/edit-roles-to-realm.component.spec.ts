import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditRolesToRealmComponent } from './edit-roles-to-realm.component';

describe('EditRolesToRealmComponent', () => {
  let component: EditRolesToRealmComponent;
  let fixture: ComponentFixture<EditRolesToRealmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditRolesToRealmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditRolesToRealmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
