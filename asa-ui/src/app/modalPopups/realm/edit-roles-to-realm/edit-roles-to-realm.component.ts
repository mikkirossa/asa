import { Component, ElementRef, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ASAPermissionService, Permission, Realm, Role } from '../../../service/admin.service';
import { CacheService } from '../../../service/cache.service';
import { CommonService } from '../../../service/common.service';
import { ErrorService } from '../../../service/error.service';
import { Constants } from '../../../utils/constants';

@Component({
  selector: 'app-edit-roles-to-realm',
  templateUrl: './edit-roles-to-realm.component.html',
  styleUrls: ['./edit-roles-to-realm.component.css']
})
export class EditRolesToRealmComponent implements OnInit, OnChanges {
  @Input() childModalData: any;
  @ViewChild('closeAddEditModal') closeAddEditModal!: ElementRef;
  role: Role = new Role();
  realm!: Realm;
  permissions: Array<any> = new Array<any>();
  componentKeyEditRolesInRealmAddPermissions = Constants.ComponentKeyEditRolesInRealmAddPermissions;
  formGroup: FormGroup = new FormGroup({
    id: new FormControl(['', Validators.required]),
    realmId: new FormControl(['', Validators.required]),
    key: new FormControl(['', Validators.required]),
    name: new FormControl(['', Validators.required]),
    description: new FormControl(['', Validators.required]),
    permissionIds: new FormControl([new Array<any>()]),
  });

  constructor(private formBuilder: FormBuilder,
    private asaPermissionService: ASAPermissionService,
    private errorInfoService: ErrorService,
    private commonService: CommonService,
    private cacheService: CacheService) {
  }

  ngOnInit(): void {
    this.bindData();
  }

  ngOnChanges() {
    console.log('new row to edit', this.childModalData);
    this.bindData();
  }

  bindData() {
    this.formGroup.setValue(this.childModalData.data);
    this.getPermissions();
  }

  getPermissions() {
    this.permissions = this.cacheService.allRoles.filter(r => r.realmId == this.childModalData.data.realmId)[0]?.roles.filter((ro: any) => ro.id == this.childModalData.data.id)[0]?.permissionIds;
    this.permissions.forEach(t => {
      let alreadySelected = this.childModalData.data.permissionIds.includes(t.id);
      t.isSelected = alreadySelected;
    });
    console.log(this.permissions);
  }

  save() {
    const self = this;
    this.role.id = this.formGroup.controls['id'].value;
    this.role.key = this.formGroup.controls['key'].value;
    this.role.name = this.formGroup.controls['name'].value;
    this.role.description = this.formGroup.controls['description'].value;
    this.role.realmId = this.childModalData.data.id;
    this.role.permissionIds = this.permissions.filter(p => p.isSelected == true).map(a => a.id);
    console.log(this.role);
    this.asaPermissionService.update_Role(this.childModalData.data.id, this.role)
      .subscribe({
        next(result) {
          self.errorInfoService.createErrorInfoSuccessObject([{ message: Constants.updateRoleStatusMessage }]);
          self.commonService.setEditRealmGridRefreshSubject(Constants.ID_Role);
        }
      });
  }
}
