import {Component, ElementRef, Input, OnInit, ViewChild} from "@angular/core";

import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {
  ASAPermissionService, IRealm,
  PrincipalStorage,
  PrincipalStorageSearchRequest, Realm,
  RealmSearchRequest
} from "../../../service/admin.service";
import {Constants} from "../../../utils/constants";

@Component({
  selector: 'app-add-realm',
  templateUrl: './add-realm.component.html',
  styleUrls: ['./add-realm.component.css']
})

export class AddRealmComponent implements OnInit {

  formGroup: FormGroup = new FormGroup({
    key: new FormControl('', [Validators.required]),
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    userStorageId: new FormControl('', [Validators.required]),
    clientStorageId: new FormControl('', [Validators.required]),
    tokenLifetimeInMinutes: new FormControl('15', [Validators.required]),
    emailAddresses: new FormControl('', [Validators.pattern(Constants.formEmailValidationExp)])
  });

  @Input() childModalData: any;
  @ViewChild('closeAddEditModal')
  closeAddEditModal!: ElementRef;
  storages: PrincipalStorage[] | undefined;
  realm: Realm = new Realm();

  constructor(private asaPermissionService: ASAPermissionService,
              private router: Router,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    const self = this;
    const request = new PrincipalStorageSearchRequest();
    this.asaPermissionService.search_For_Storage(request)
      .subscribe({
        next(result) {
          self.storages = result.items;
          console.log('Storage is retrieved', result.items)
        }
      });
    this.createForm();
  }

  createForm() {
    this.formGroup = this.formBuilder.group({
      key: ['', [Validators.required]],
      name: ['', Validators.required],
      description: ['', Validators.required],
      userStorageId: ['', Validators.required],
      clientStorageId: ['', Validators.required],
      tokenLifetimeInMinutes: ['15', Validators.required],
      emailAddresses: ['', Validators.pattern(Constants.formEmailValidationExp)]
    });
  }

  addRealm(action: string) {
    const self = this;
    this.realm.key = this.formGroup.controls['key'].value;
    this.realm.name = this.formGroup.controls['name'].value;
    this.realm.description = this.formGroup.controls['description'].value;
    this.realm.userStorageId = this.formGroup.controls['userStorageId'].value;
    this.realm.clientStorageId = this.formGroup.controls['clientStorageId'].value;
    this.realm.tokenLifetimeInMinutes = parseInt(this.formGroup.controls['tokenLifetimeInMinutes'].value);
    this.realm.emailAddresses = this.formGroup.controls['emailAddresses'].value;

    console.log('Realm object ', this.realm);

    this.asaPermissionService.add_Realm(this.realm)
      .subscribe({
        next(realmId) {
          console.log('Realm Id inside next ', realmId)
          if(action === 'route')
            self.router.navigate(['/realms', realmId, 'edit']);
        }
      });
    this.closeAddEditModal.nativeElement.click();
  }


}
