import {Component, ElementRef, Input, OnChanges, OnInit, ViewChild} from "@angular/core";
import {
  ASAPermissionService,
  ClientPrincipal,
  ClientPrincipalSearchRequest, Role,
} from "../../../service/admin.service";
import {Constants} from "../../../utils/constants";
import {concatMap, forkJoin, Observable} from "rxjs";
import {ErrorService} from "../../../service/error.service";
import {CacheService} from "../../../service/cache.service";

@Component({
  selector: 'app-select-client',
  templateUrl: './add-clients-to-realm.component.html'
})

export class AddClientsToRealmComponent implements OnInit, OnChanges {

  @Input() childModalData: any;
  @ViewChild('closeAddEditModal') closeAddEditModal!: ElementRef;
  colDefKeyAddClientsToRealm = Constants.ComponentKeyAddClientsToRealm;
  itemRowData: ClientPrincipal[] | undefined;
  allClients: ClientPrincipal[] | undefined;
  gridDataFetching: number = 0;
  availableRoles!: Role[];

  constructor(private asaPermissionService: ASAPermissionService,
              private cacheService: CacheService,
              private errorInfoService: ErrorService) {
  }

  ngOnInit() {
  }

  ngOnChanges() {
    this.gridDataFetching++
    setTimeout(() => {
      this.gridDataFetching++;
    }, 1000);
    console.log(this.childModalData);
    this.loadRoles();
  }

  searchForClients() {
    this.itemRowData = [];
    let clientRequest = new ClientPrincipalSearchRequest();
    const self = this;

    this.asaPermissionService.search_For_Clients(clientRequest)
      .subscribe({
        next(all) {
          console.log('All Clients', all.items);
          self.allClients = all.items;
          clientRequest = new ClientPrincipalSearchRequest();
          clientRequest.realmId = self.childModalData.data.id;
          self.asaPermissionService.search_For_Clients(clientRequest)
            .subscribe({
                next(existing) {
                  existing.items?.forEach((f:any) =>
                    (<any>self.allClients).splice((<any>self.allClients).findIndex((e: any) => e.id === f.id), 1));
                  self.itemRowData = self.allClients;
                  self.itemRowData = (<any>self.itemRowData).map((e: any) => ({
                    ...e,
                    isSelected: false,
                    roles: self.availableRoles,
                    realmId: self.childModalData.data.id,
                    selectedRoles: new Array<any>()
                  }));
                }
              }
            )
        }
      })
  }

  loadRoles() {
    if (this.cacheService.allRoles.length == 0 || this.cacheService.allRoles == null || this.cacheService.allRoles == undefined) {
      setTimeout(() => {
        this.loadRoles();
      }, 500);
    } else {
      console.log('this.cacheService.allRoles', this.cacheService.allRoles);
      this.availableRoles = this.cacheService.allRoles.filter(p => p.realmId == this.childModalData.data.id)[0].roles;
      console.log('this.cacheService.availableRoles', this.availableRoles);
      this.searchForClients();
    }
  }

  save() {
    console.log('itemRowData', this.itemRowData);
    let obsForRolesArray: Array<Observable<any>> = new Array<Observable<any>>();
    let validationFail: boolean = false;
    (<any>this.itemRowData).forEach((p: any) => {
      if (p.isSelected == true) {
        // validate if selected role is present
        if (p.selectedRoles.length == 0 || p.selectedRoles == undefined || p.selectedRoles == null) {
          this.errorInfoService.createErrorInfoSuccessObject([{message: Constants.selectRolesForClientsMessage}]);
          validationFail = true;
          return;
        }
        p.selectedRoles.forEach((r: any) => {
          obsForRolesArray.push(this.asaPermissionService.add_Client_To_Role(p.id, p.realmId, r.split('-')[0]));
        });
      }
    });
    console.log('obsForRolesArray', obsForRolesArray);
    if (obsForRolesArray.length > 0 && validationFail == false)
      this.addClientToRole(obsForRolesArray);
  }

  addClientToRole(obsForRolesArray: Array<Observable<any>>) {
    forkJoin(obsForRolesArray)
      .pipe(
        concatMap(item$ => item$),
      )
      .subscribe((obj: any) => {
          console.log('adding client to role', obj);
        },
        error => {
          console.log('Error while adding client to role');
        }, () => {
          console.log('Done... adding client to role');
          this.itemRowData = (<any>this.itemRowData).filter((p: any) => p.isSelected != true)
          this.errorInfoService.createErrorInfoSuccessObject([{message: Constants.addUserStatusMessage}]);
          this.closeAddEditModal.nativeElement.click();
        });

  }

}
