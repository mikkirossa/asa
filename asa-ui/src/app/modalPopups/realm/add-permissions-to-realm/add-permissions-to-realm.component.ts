import {Component, ElementRef, Input, OnInit, ViewChild} from "@angular/core";
import {ASAPermissionService, Permission, Realm} from "../../../service/admin.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {ErrorService} from "../../../service/error.service";
import {CommonService} from "../../../service/common.service";
import {Constants} from "../../../utils/constants";

@Component({
  selector: 'app-add-permissions-to-realm',
  templateUrl: './add-permissions-to-realm.component.html'
})

export class AddPermissionsToRealmComponent implements OnInit {
  @Input() childModalData: any;
  @ViewChild('closeAddEditModal') closeAddEditModal!: ElementRef;
  permission: Permission = new Permission();
  realm!: Realm;

  formGroup: FormGroup = new FormGroup({
    key: new FormControl(['', Validators.required]),
    name: new FormControl(['', Validators.required]),
    description: new FormControl(['', Validators.required]),
  });


  constructor(private formBuilder: FormBuilder,
              private activatedRoute: ActivatedRoute,
              private asaPermissionService: ASAPermissionService,
              private errorInfoService: ErrorService,
              private commonService: CommonService) {
  }

  ngOnInit() {
    this.createForm();
  }


  createForm() {
    this.formGroup = this.formBuilder.group({
      key: [this.permission.key, Validators.required],
      name: [this.permission.name, Validators.required],
      description: [this.permission.description, Validators.required],
    });
  }

  save() {
    const self = this;
    this.permission.key = this.formGroup.controls['key'].value;
    this.permission.name = this.formGroup.controls['name'].value;
    this.permission.description = this.formGroup.controls['description'].value;
    this.permission.realmId = this.childModalData.data.id;

    this.asaPermissionService.add_Permission(this.permission)
      .subscribe({
        next(result) {
          self.errorInfoService.createErrorInfoSuccessObject([{ message: Constants.addPermissionStatusMessage}]);
          // self.commonService.setEditRealmGridRefreshSubject(Constants.ID_User);
          self.closeAddEditModal.nativeElement.click();
        }
      });
  }
}
