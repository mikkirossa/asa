import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUsersToRealmComponent } from './add-users-to-realm.component';

describe('AddUsersToRealmComponent', () => {
  let component: AddUsersToRealmComponent;
  let fixture: ComponentFixture<AddUsersToRealmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddUsersToRealmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUsersToRealmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
