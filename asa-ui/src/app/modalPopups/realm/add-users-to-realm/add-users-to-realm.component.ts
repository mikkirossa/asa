import { Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import { concatMap, forkJoin, Observable } from 'rxjs';
import { Message } from '../../../models/common/validationCriteria';
import { ASAPermissionService, RealmSearchRequest, Role, UserPrincipal, UserPrincipalSearchRequest } from '../../../service/admin.service';
import { CacheService } from '../../../service/cache.service';
import { CommonService } from '../../../service/common.service';
import { ErrorService } from '../../../service/error.service';
import { Constants } from '../../../utils/constants';

@Component({
  selector: 'app-add-users-to-realm',
  templateUrl: './add-users-to-realm.component.html',
  styleUrls: ['./add-users-to-realm.component.css']
})
export class AddUsersToRealmComponent implements OnInit {
  @Input() childModalData: any;
  @ViewChild('closeAddEditModal') closeAddEditModal!: ElementRef;
  itemRowData: UserPrincipal[] | undefined;
  allUsers: UserPrincipal[] | undefined;
  colDefKeyAddUsersToRealm = Constants.ComponentKeyAddUsersToRealm;
  gridDataFetching: number = 0;
  availableRoles!: Role[];

  constructor(private asaPermissionService: ASAPermissionService,
    private cacheService: CacheService, private errorInfoService: ErrorService, public commonService: CommonService,) {
  }

  ngOnChanges() {
    this.gridDataFetching++;
    setTimeout(() => {
      this.gridDataFetching++;
    }, 1000);
    console.log(this.childModalData);
    this.loadRoles();
  }

  ngOnInit(): void {
  }

  searchForUsers() {
    this.itemRowData = [];
    let userRequest = new UserPrincipalSearchRequest();

    const self = this;
    this.asaPermissionService.search_For_Users(userRequest)
      .subscribe({
        next(all) {
          self.allUsers = all.items;
          userRequest = new UserPrincipalSearchRequest();
          userRequest.realmId = self.childModalData.data.id;
          self.asaPermissionService.search_For_Users(userRequest)
            .subscribe({
              next(existing) {
                existing.items?.forEach((f: any) =>
                  (<any>self.allUsers).splice((<any>self.allUsers).findIndex((e: any) => e.id === f.id), 1));
                self.itemRowData = self.allUsers;
                self.itemRowData = (<any>self.itemRowData).map((e: any) => ({
                  ...e,
                  isSelected: false,
                  roles: self.availableRoles,
                  realmId: self.childModalData.data.id,
                  selectedRoles: new Array<any>()
                }));
              }
            });
        }
      });
  }

  loadRoles() {
    if (this.cacheService.allRoles.length == 0 || this.cacheService.allRoles == null || this.cacheService.allRoles == undefined) {
      setTimeout(() => {
        this.loadRoles();
      }, 500);
    } else {
      console.log('this.cacheService.allRoles', this.cacheService.allRoles);
      this.availableRoles = this.cacheService.allRoles.filter(p => p.realmId == this.childModalData.data.id)[0].roles;
      console.log('this.cacheService.availableRoles', this.availableRoles);

      this.searchForUsers();
    }
  }

  save() {
    console.log('itemRowData', this.itemRowData);
    let obsForRolesArray: Array<Observable<any>> = new Array<Observable<any>>();
    let validationFail: boolean = false;
    (<any>this.itemRowData).forEach((p: any) => {

      if (p.isSelected == true) {
        console.log('This is single user isSelected from array', p)
        // validate if selected role is present
        if (p.selectedRoles == undefined || p.selectedRoles == null || p.selectedRoles.length == 0) {
          this.errorInfoService.createErrorInfoSuccessObject([{message: Constants.selectRolesForUsersMessage}]);
          validationFail = true;
          return;
        }
        p.selectedRoles.forEach((r: any) => {
          obsForRolesArray.push(this.asaPermissionService.add_User_To_Role(p.id, p.realmId, r.split('-')[0]));
        });
      }

    });
    console.log('obsForRolesArray', obsForRolesArray);
    if (obsForRolesArray.length > 0 && validationFail == false)
      this.addUserToRole(obsForRolesArray);
  }

  cancel() {
  }

  addUserToRole(obsForRolesArray: Array<Observable<any>>) {
    forkJoin(obsForRolesArray)
      .pipe(
        concatMap(item$ => item$),
      )
      .subscribe((obj: any) => {
          console.log('adding user to role', obj);
        },
        error => {
          console.log('Error while adding user to role');
        }, () => {
          console.log('Done... adding user to role');
          this.itemRowData = (<any>this.itemRowData).filter((p: any) => p.isSelected != true)

          this.errorInfoService.createErrorInfoSuccessObject([{ message: Constants.addUserStatusMessage}]);
            this.commonService.setEditRealmGridRefreshSubject(Constants.ID_User);
            this.closeAddEditModal.nativeElement.click();
        });

  }
}
