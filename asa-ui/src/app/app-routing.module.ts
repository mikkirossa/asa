import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './admin/home/home.component';
import { LoginComponent } from './admin/login/login.component';
import { SettingsComponent } from './admin/settings/settings.component';
import { UserAddComponent } from './admin/users/user-add/user-add.component';
import { UserEditComponent } from './admin/users/user-edit/user-edit.component';
import { UserSearchComponent } from './admin/users/user-search/user-search.component';
import { UserViewComponent } from './admin/users/user-view/user-view.component';
import { AuthGuard } from './service/auth-guard.service';
import {ClientSearchComponent} from "./admin/clients/client-search/client-search.component";
import {ClientAddComponent} from "./admin/clients/client-add/client-add.component";
import {ClientEditComponent} from "./admin/clients/client-edit/client-edit.component";
import { RealmEditComponent } from './admin/realm/realm-edit/realm-edit.component';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard], data: { layout: true, breadCrumb: '' } },
  { path: 'login', component: LoginComponent, data: { layout: false, breadCrumb: '' } },
  { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard], data: { layout: true, breadCrumb: 'Settings' } },

  { path: 'users/:principalid/edit', component: UserEditComponent, canActivate: [AuthGuard], data: { layout: true, breadCrumb: 'Edit User' } },
  { path: 'users/:principalid/view', component: UserViewComponent, canActivate: [AuthGuard], data: { layout: true, breadCrumb: 'View User' } },
  { path: 'users', component: UserSearchComponent, canActivate: [AuthGuard], data: { layout: true, breadCrumb: 'Search Users' } },
  { path: 'users/search', component: UserSearchComponent, canActivate: [AuthGuard], data: { layout: true, breadCrumb: 'Search Users' } },
  { path: 'users/add', component: UserAddComponent, canActivate: [AuthGuard], data: { layout: true, breadCrumb: 'Add User' } },

  { path: 'clients/:principalid/edit', component: ClientEditComponent, canActivate: [AuthGuard], data: { layout: true, breadCrumb: 'Edit Client' } },
  { path: 'clients', component: ClientSearchComponent, canActivate: [AuthGuard], data: { layout: true, breadCrumb: 'Search Clients' } },
  { path: 'clients/search', component: ClientSearchComponent, canActivate: [AuthGuard], data: {layout: true, breadCrumb: 'Search Clients'}},
  { path: 'clients/add', component: ClientAddComponent, canActivate: [AuthGuard], data: {layout: true, breadCrumb: 'Add Client'}},

  { path: 'realms/:realmId/edit', component: RealmEditComponent, canActivate: [AuthGuard], data: { layout: true, breadCrumb: 'Edit Realm' } },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
