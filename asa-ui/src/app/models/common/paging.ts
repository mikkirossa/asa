export interface Paging<T> {
  totalRecords: number;
  recordsPerPage: number;
  page: number;
  items: T[];
}
