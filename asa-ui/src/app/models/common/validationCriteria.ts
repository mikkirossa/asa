import { Constants } from "../../utils/constants";

export class UniqueValidationCriteria {
  constructor() {
    this.type = Constants.messageType_ERROR;
  }
  name: string | undefined;
  type: string | undefined;
  matchingFields: number | undefined;
  pkField: string | undefined;
  pkValue: string | undefined;
  field1: string | undefined;
  field1Value: string | undefined;
  field2: string | undefined;
  field2Value: string | undefined;
  field3: string | undefined;
  field3Value: string | undefined;
  field4: string | undefined;
  field4Value: string | undefined;
  status: boolean | undefined;
  message: string | undefined;
}

export class Message { // it can be info, exception
  message: string | undefined;
  type: string | undefined; // error, info, success
  items: Array<any> | undefined; // Array of errors, info, success
}

export class Information {
  constructor() {
    this.type = Constants.messageType_INFO;
  }
  name: string | undefined;
  type: string | undefined; // error, info, success
  message: string | undefined;
}

