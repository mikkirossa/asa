export class Config {
  public static Permissions: any = {
    CONFIG_ADMIN: 'CONFIG_ADMIN',
    ACC_R: 'ACC_R',
    ACC_W: 'ACC_W',
    ACC_D: 'ACC_D',
    APPR_TY_R: 'APPR_TY_R',
    APPR_TY_W: 'APPR_TY_W',
    APPR_TY_D: 'APPR_TY_D',
    PAY_TY_R: 'PAY_TY_R',
    PAY_TY_W: 'PAY_TY_W',
    PAY_TY_D: 'PAY_TY_D',
    SET_GR_R: 'SET_GR_R',
    SET_GR_W: 'SET_GR_W',
    SET_GR_D: 'SET_GR_D',
    INV_SET_ST_R: 'INV_SET_ST_R',
    INV_SET_ST_W: 'INV_SET_ST_W',
    INV_SET_ST_D: 'INV_SET_ST_D',
    AUTH_ST_R: 'AUTH_ST_R',
    AUTH_ST_W: 'AUTH_ST_W',
    AUTH_ST_D: 'AUTH_ST_D',
    MOD_R: 'MOD_R',
    MOD_W: 'MOD_W',
    MOD_D: 'MOD_D',
    AIRP_R: 'AIRP_R',
    AIRP_W: 'AIRP_W',
    AIRP_D: 'AIRP_D',
    GATW_R: 'GATW_R',
    GATW_W: 'GATW_W',
    GATW_D: 'GATW_D',
    GATW_CONF_R: 'GATW_CONF_R',
    GATW_CONF_W: 'GATW_CONF_W',
    GATW_CONF_D: 'GATW_CONF_D',
    MER_R: 'MER_R',
    MER_W: 'MER_W',
    MER_D: 'MER_D',
    CUST_MES_R: 'CUST_MES_R',
    CUST_MES_W: 'CUST_MES_W',
    CUST_MES_D: 'CUST_MES_D',
    GATW_RESP_R: 'GATW_RESP_R',
    GATW_RESP_W: 'GATW_RESP_W',
    GATW_RESP_D: 'GATW_RESP_D',
    GATW_RESP_CUST_MES_R: 'GATW_RESP_CUST_MES_R',
    GATW_RESP_CUST_MES_W: 'GATW_RESP_CUST_MES_W',
    GATW_RESP_CUST_MES_D: 'GATW_RESP_CUST_MES_D',
    MAP_TY_R: 'MAP_TY_R',
    MAP_TY_W: 'MAP_TY_W',
    MAP_TY_D: 'MAP_TY_D',
    CLI_PROD_R: 'CLI_PROD_R',
    CLI_PROD_W: 'CLI_PROD_W',
    CLI_PROD_D: 'CLI_PROD_D',
    GATW_PROD_R: 'GATW_PROD_R',
    GATW_PROD_W: 'GATW_PROD_W',
    GATW_PROD_D: 'GATW_PROD_D',
    GATW_CLI_PROD_MAP_R: 'GATW_CLI_PROD_MAP_R',
    GATW_CLI_PROD_MAP_W: 'GATW_CLI_PROD_MAP_W',
    GATW_CLI_PROD_MAP_D: 'GATW_CLI_PROD_MAP_D',
    PROD_TY_R: 'PROD_TY_R',
    PROD_TY_W: 'PROD_TY_W',
    PROD_TY_D: 'PROD_TY_D',
    QUANT_TY_R: 'QUANT_TY_R',
    QUANT_TY_W: 'QUANT_TY_W',
    QUANT_TY_D: 'QUANT_TY_D',
    RATE_TY_R: 'RATE_TY_R',
    RATE_TY_W: 'RATE_TY_W',
    RATE_TY_D: 'RATE_TY_D',
    UOM_R: 'UOM_R',
    UOM_W: 'UOM_W',
    UOM_D: 'UOM_D',
    USR_SET_R: 'USR_SET_R',
    USR_SET_W: 'USR_SET_W',
    USR_SET_D: 'USR_SET_D',
    INV_META_TY_R: 'INV_META_TY_R',
    INV_META_TY_W: 'INV_META_TY_W',
    INV_META_TY_D: 'INV_META_TY_D',
    INV_COMP_META_TY_R: 'INV_COMP_META_TY_R',
    INV_COMP_META_TY_W: 'INV_COMP_META_TY_W',
    INV_COMP_META_TY_D: 'INV_COMP_META_TY_D',
    CLI_TX_R: 'CLI_TX_R',
    CLI_TX_W: 'CLI_TX_W',
    CLI_TX_D: 'CLI_TX_D',
    GATW_TX_R: 'GATW_TX_R',
    GATW_TX_W: 'GATW_TX_W',
    GATW_TX_D: 'GATW_TX_D',
    GATW_CLI_TX_R: 'GATW_CLI_TX_R',
    GATW_CLI_TX_W: 'GATW_CLI_TX_W',
    GATW_CLI_TX_D: 'GATW_CLI_TX_D',
    TEND_TY_R: 'TEND_TY_R',
    TEND_TY_W: 'TEND_TY_W',
    TEND_TY_D: 'TEND_TY_D',
    ORA_SIG_BS_R: 'ORA_SIG_BS_R',
    ORA_SIG_BS_W: 'ORA_SIG_BS_W',
    ORA_SIG_BS_D: 'ORA_SIG_BS_D',
    AIRP_PT_MER_R: 'AIRP_PT_MER_R',
    AIRP_PT_MER_W: 'AIRP_PT_MER_W',
    AIRP_PT_MER_D: 'AIRP_PT_MER_D',
  }

  public static Permissions1: any = [
    {
      "Name": "CONFIG_ADMIN",
      "Description": "Config api read, write, delete permissions for all endpoints",
      "Key": "CONFIG_ADMIN"
    },
    {
      "Name": "ACCOUNTING_READ",
      "Description": "Accounting Read permission",
      "Key": "ACC_R"
    },
    {
      "Name": "ACCOUNTING_WRITE",
      "Description": "Accounting Insert and Update permissions",
      "Key": "ACC_W"
    },
    {
      "Name": "ACCOUNTING_WRITE",
      "Description": "Accounting Delete permission",
      "Key": "ACC_D"
    },
    {
      "Name": "APPROVAL_TYPES_READ",
      "Description": "Approval Types Read permission",
      "Key": "APPR_TY_R"
    },
    {
      "Name": "APPROVAL_TYPES_WRITE",
      "Description": "Approval Types Insert and Update permissions",
      "Key": "APPR_TY_W"
    },
    {
      "Name": "APPROVAL_TYPES_DELETE",
      "Description": "Approval Types Delete permission",
      "Key": "APPR_TY_D"
    },
    {
      "Name": "PAYMENT_TYPES_READ",
      "Description": "Payment Types Read permission",
      "Key": "PAY_TY_R"
    },
    {
      "Name": "PAYMENT_TYPES_WRITE",
      "Description": "Payment Types Insert and Update permissions",
      "Key": "PAY_TY_W"
    },
    {
      "Name": "PAYMENT_TYPES_DELETE",
      "Description": "Payment Types Delete permission",
      "Key": "PAY_TY_D"
    },
    {
      "Name": "SETTLEMENT_GROUPS_READ",
      "Description": "Settlement Groups Read permission",
      "Key": "SET_GR_R"
    },
    {
      "Name": "SETTLEMENT_GROUPS_WRITE",
      "Description": "Settlement Groups Insert and Update permissions",
      "Key": "SET_GR_W"
    },
    {
      "Name": "SETTLEMENT_GROUPS_DELETE",
      "Description": "Settlement Groups Delete permission",
      "Key": "SET_GR_D"
    },
    {
      "Name": "INVOICE_SETTLEMENT_STATUSES_READ",
      "Description": "Invoice Settlement Status Read permission",
      "Key": "INV_SET_ST_R"
    },
    {
      "Name": "INVOICE_SETTLEMENT_STATUSES_WRITE",
      "Description": "Invoice Settlement Status Insert and Update permissions",
      "Key": "INV_SET_ST_W"
    },
    {
      "Name": "INVOICE_SETTLEMENT_STATUSES_DELETE",
      "Description": "Invoice Settlement Status Delete permission",
      "Key": "INV_SET_ST_D"
    },
    {
      "Name": "AUTHORIZATION_STATUSES_READ",
      "Description": "Authorization Status Read permission",
      "Key": "AUTH_ST_R"
    },
    {
      "Name": "AUTHORIZATION_STATUSES_WRITE",
      "Description": "Authorization Status Insert and Update permissions",
      "Key": "AUTH_ST_W"
    },
    {
      "Name": "AUTHORIZATION_STATUSES_DELETE",
      "Description": "Authorization Status Delete permission",
      "Key": "ACC_D"
    },
    {
      "Name": "MODULES_READ",
      "Description": "Modules Read permission",
      "Key": "MOD_R"
    },
    {
      "Name": "MODULES_WRITE",
      "Description": "Modules Insert and Update permissions",
      "Key": "MOD_W"
    },
    {
      "Name": "MODULES_DELETE",
      "Description": "Modules Delete permission",
      "Key": "MOD_D"
    },
    {
      "Name": "AIRPORTS_READ",
      "Description": "Airport Read permission",
      "Key": "AIRP_R"
    },
    {
      "Name": "AIRPORTS_WRITE",
      "Description": "Airport Insert and Update permissions",
      "Key": "AIRP_W"
    },
    {
      "Name": "AIRPORTS_DELETE",
      "Description": "Airport Delete permission",
      "Key": "AIRP_D"
    },
    {
      "Name": "GATEWAYS_READ",
      "Description": "Gateways Read permission",
      "Key": "GATW_R"
    },
    {
      "Name": "GATEWAYS_WRITE",
      "Description": "Gateways Insert and Update permissions",
      "Key": "GATW_W"
    },
    {
      "Name": "GATEWAYS_DELETE",
      "Description": "Gateways Delete permission",
      "Key": "GATW_D"
    },
    {
      "Name": "GATEWAY_CONFIGURATION_READ",
      "Description": "Gateway configuration Read permission",
      "Key": "GATW_CONF_R"
    },
    {
      "Name": "GATEWAY_CONFIGURATION_WRITE",
      "Description": "Gateway configuration Insert and Update permissions",
      "Key": "GATW_CONF_W"
    },
    {
      "Name": "GATEWAY_CONFIGURATION_DELETE",
      "Description": "Gateway configuration Delete permission",
      "Key": "GATW_CONF_D"
    },
    {
      "Name": "MERCHANTS_READ",
      "Description": "Merchant Read permission",
      "Key": "MER_R"
    },
    {
      "Name": "MERCHANTS_WRITE",
      "Description": "Merchant Insert and Update permissions",
      "Key": "MER_W"
    },
    {
      "Name": "MERCHANTS_DELETE",
      "Description": "Merchant Delete permission",
      "Key": "MER_D"
    },
    {
      "Name": "CUSTOM_MESSAGES_READ",
      "Description": "Custom Message Read permission",
      "Key": "CUST_MES_R"
    },
    {
      "Name": "CUSTOM_MESSAGES_WRITE",
      "Description": "Custom Message Insert and Update permissions",
      "Key": "CUST_MES_W"
    },
    {
      "Name": "CUSTOM_MESSAGES_DELETE",
      "Description": "Custom Message Delete permission",
      "Key": "CUST_MES_D"
    },
    {
      "Name": "GATEWAY_RESPONSES_READ",
      "Description": "Gateway Response Read permission",
      "Key": "GATW_RESP_R"
    },
    {
      "Name": "GATEWAY_RESPONSES_WRITE",
      "Description": "Gateway Response Insert and Update permissions",
      "Key": "GATW_RESP_W"
    },
    {
      "Name": "GATEWAY_RESPONSES_DELETE",
      "Description": "Gateway Response Delete permission",
      "Key": "GATW_RESP_D"
    },
    {
      "Name": "GATEWAY_RESPONSE_CUSTOM_MESSAGES_READ",
      "Description": "Gateway Response Custom Message Read permission",
      "Key": "GATW_RESP_CUST_MES_R"
    },
    {
      "Name": "GATEWAY_RESPONSE_CUSTOM_MESSAGES_WRITE",
      "Description": "Gateway Response Custom Message Insert and Update permissions",
      "Key": "GATW_RESP_CUST_MES_W"
    },
    {
      "Name": "GATEWAY_RESPONSE_CUSTOM_MESSAGES_DELETE",
      "Description": "Gateway Response Custom Message Delete permission",
      "Key": "GATW_RESP_CUST_MES_D"
    },
    {
      "Name": "MAPPING_TYPES_READ",
      "Description": "Mapping Types Read permission",
      "Key": "MAP_TY_R"
    },
    {
      "Name": "MAPPING_TYPES_WRITE",
      "Description": "Mapping Types Insert and Update permissions",
      "Key": "MAP_TY_W"
    },
    {
      "Name": "MAPPING_TYPES_DELETE",
      "Description": "Mapping Types Delete permission",
      "Key": "MAP_TY_D"
    },
    {
      "Name": "CLIENT_PRODUCTS_READ",
      "Description": "Client Products Read permission",
      "Key": "CLI_PROD_R"
    },
    {
      "Name": "CLIENT_PRODUCTS_WRITE",
      "Description": "Client Products Insert and Update permissions",
      "Key": "CLI_PROD_W"
    },
    {
      "Name": "CLIENT_PRODUCTS_DELETE",
      "Description": "Client Products Delete permission",
      "Key": "CLI_PROD_D"
    },
    {
      "Name": "GATEWAY_PRODUCTS_READ",
      "Description": "Merchant Read permission",
      "Key": "GATW_PROD_R"
    },
    {
      "Name": "GATEWAY_PRODUCTS_WRITE",
      "Description": "Merchant Insert and Update permissions",
      "Key": "GATW_PROD_W"
    },
    {
      "Name": "GATEWAY_PRODUCTS_DELETE",
      "Description": "Merchant Delete permission",
      "Key": "GATW_PROD_D"
    },
    {
      "Name": "GATEWAY_CLIENT_PRODUCTS_MAPPING_READ",
      "Description": "Merchant Read permission",
      "Key": "GATW_CLI_PROD_MAP_R"
    },
    {
      "Name": "GATEWAY_CLIENT_PRODUCTS_MAPPING_WRITE",
      "Description": "Merchant Insert and Update permissions",
      "Key": "GATW_CLI_PROD_MAP_W"
    },
    {
      "Name": "GATEWAY_CLIENT_PRODUCTS_MAPPING_DELETE",
      "Description": "Merchant Delete permission",
      "Key": "GATW_CLI_PROD_MAP_D"
    },
    {
      "Name": "PRODUCT_TYPES_READ",
      "Description": "Product Types Read permission",
      "Key": "PROD_TY_R"
    },
    {
      "Name": "PRODUCT_TYPES_WRITE",
      "Description": "Product Types Insert and Update permissions",
      "Key": "PROD_TY_W"
    },
    {
      "Name": "PRODUCT_TYPES_DELETE",
      "Description": "Product Types Delete permission",
      "Key": "PROD_TY_D"
    },
    {
      "Name": "QUANTITY_TYPES_READ",
      "Description": "Quantity Types Read permission",
      "Key": "QUANT_TY_R"
    },
    {
      "Name": "QUANTITY_TYPES_WRITE",
      "Description": "Quantity Types Insert and Update permissions",
      "Key": "QUANT_TY_W"
    },
    {
      "Name": "QUANTITY_TYPES_DELETE",
      "Description": "Quantity Types Delete permission",
      "Key": "QUANT_TY_D"
    },
    {
      "Name": "RATE_TYPES_READ",
      "Description": "Rate Types Read permission",
      "Key": "RATE_TY_R"
    },
    {
      "Name": "RATE_TYPES_WRITE",
      "Description": "Rate Types Insert and Update permissions",
      "Key": "RATE_TY_W"
    },
    {
      "Name": "RATE_TYPES_DELETE",
      "Description": "Rate Types Delete permission",
      "Key": "RATE_TY_D"
    },
    {
      "Name": "UNITOFMEASURES_READ",
      "Description": "Unit of Measure Read permission",
      "Key": "UOM_R"
    },
    {
      "Name": "UNITOFMEASURES_WRITE",
      "Description": "Unit of Measure Insert and Update permissions",
      "Key": "UOM_W"
    },
    {
      "Name": "UNITOFMEASURES_DELETE",
      "Description": "Unit of Measure Delete permission",
      "Key": "UOM_D"
    },
    {
      "Name": "USERSETTINGS_READ",
      "Description": "User Settings Read permission",
      "Key": "USR_SET_R"
    },
    {
      "Name": "USERSETTINGS_WRITE",
      "Description": "User Settings Insert and Update permissions",
      "Key": "USR_SET_W"
    },
    {
      "Name": "USERSETTINGS_DELETE",
      "Description": "User Settings Delete permission",
      "Key": "USR_SET_D"
    },
    {
      "Name": "INVOICEMETADATATYPES_READ",
      "Description": "Invoice Metadata Types Read permission",
      "Key": "INV_META_TY_R"
    },
    {
      "Name": "INVOICEMETADATATYPES_WRITE",
      "Description": "Invoice Metadata Types Insert and Update permissions",
      "Key": "INV_META_TY_W"
    },
    {
      "Name": "INVOICEMETADATATYPES_DELETE",
      "Description": "Invoice Metadata Types Delete permission",
      "Key": "INV_META_TY_D"
    },
    {
      "Name": "INVOICECOMPONENTMETADATATYPES_READ",
      "Description": "Invoice Component Metadata Types Read permission",
      "Key": "INV_COMP_META_TY_R"
    },
    {
      "Name": "INVOICECOMPONENTMETADATATYPES_WRITE",
      "Description": "Invoice Component Metadata Types Insert and Update permissions",
      "Key": "INV_COMP_META_TY_W"
    },
    {
      "Name": "INVOICECOMPONENTMETADATATYPES_DELETE",
      "Description": "Invoice Component Metadata Types Delete permission",
      "Key": "INV_COMP_META_TY_D"
    },
    {
      "Name": "CLIENT_TAXES_READ",
      "Description": "Client Tax Read permission",
      "Key": "CLI_TX_R"
    },
    {
      "Name": "CLIENT_TAXES_WRITE",
      "Description": "Client Tax Insert and Update permissions",
      "Key": "CLI_TX_W"
    },
    {
      "Name": "CLIENT_TAXES_DELETE",
      "Description": "Client Tax Delete permission",
      "Key": "CLI_TX_D"
    },
    {
      "Name": "GATEWAY_TAXES_READ",
      "Description": "Gateway Tax Read permission",
      "Key": "GATW_TX_R"
    },
    {
      "Name": "GATEWAY_TAXES_WRITE",
      "Description": "Gateway Tax Insert and Update permissions",
      "Key": "GATW_TX_W"
    },
    {
      "Name": "GATEWAY_TAXES_DELETE",
      "Description": "Gateway Tax Delete permission",
      "Key": "GATW_TX_D"
    },
    {
      "Name": "GATEWAY_CLIENT_TAXES_MAPPING_READ",
      "Description": "Gateway Client Tax Mapping Read permission",
      "Key": "GATW_CLI_TX_R"
    },
    {
      "Name": "GATEWAY_CLIENT_TAXES_MAPPING_WRITE",
      "Description": "Gateway Client Tax Mapping Insert and Update permissions",
      "Key": "GATW_CLI_TX_W"
    },
    {
      "Name": "GATEWAY_CLIENT_TAXES_MAPPING_DELETE",
      "Description": "Gateway Client Tax Mapping Delete permission",
      "Key": "GATW_CLI_TX_D"
    },
    {
      "Name": "TENDER_TYPES_READ",
      "Description": "Tender Types Read permission",
      "Key": "TEND_TY_R"
    },
    {
      "Name": "TENDER_TYPES_WRITE",
      "Description": "Tender Types Insert and Update permissions",
      "Key": "TEND_TY_W"
    },
    {
      "Name": "TENDER_TYPES_DELETE",
      "Description": "Tender Types Delete permission",
      "Key": "TEND_TY_D"
    },
    {
      "Name": "ORACLESIGNETBASE_READ",
      "Description": "Oracle Signet Base mapping Read permission",
      "Key": "ORA_SIG_BS_R"
    },
    {
      "Name": "ORACLESIGNETBASE_WRITE",
      "Description": "Oracle Signet Base mapping Insert and Update permissions",
      "Key": "ORA_SIG_BS_W"
    },
    {
      "Name": "ORACLESIGNETBASE_DELETE",
      "Description": "Oracle Signet Base mapping Delete permission",
      "Key": "ORA_SIG_BS_D"
    }];

}
