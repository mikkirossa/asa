
export class Constants {
  public static AppName: string = "Application Security Administration";
  public static AppPathSeparator: string = " / ";
  public static Permission_Keys: string = "per-keys";
  public static User_Settings: string = "userSettings";
  public static User_SettingsKey: string = "ASA";
  public static LoginName: string = "loginName";

  public static User_SettingProps: Array<string> = ["doNotShowAddPopUp-false", "doNotShowSavePopUp-false","minimizeSidebar-false"];

  public static AppDateFormat: string = 'MMM dd, yy, HH:mm a';

  public static deleteMessage = "Are you sure you want to delete PRIMARYKEY <b>PRIMARYVALUE</b> ?";
  public static deleteSuccessMessage = "PRIMARYKEY <b>PRIMARYVALUE</b> is successfully deleted.";

  public static addUserStatusMessage = "Selected user(s) added successfully.";
  public static addClientStatusMessage = "Selected user(s) added successfully.";
  public static addRoleStatusMessage = "Role has been added successfully.";
  public static updateRoleStatusMessage = "Role has been updated successfully.";
  public static addPermissionStatusMessage = "Permission has been added successfully.";

  public static selectRolesForUsersMessage = "Please select role(s) for selected user(s).";
  public static selectRolesForClientsMessage = "Please select role(s) for selected client(s).";

  static validation = class {
    public textMessage: string = "Please fill this out.";
    public ddlMessage: string = "Please make your selection.";
    public cbMessage: string = "Please make your selection.";
    public rbMessage: string = "Please make your selection.";
    public phonePatternMessage: string = "Invalid phone, should be 10 digits.";
    public zipPatternMessage: string = "Invalid zip, should be 5 digits.";
  }

  static addClientOrUserKey = class {
    public client: string = "client";
    public user: string = "user";

  }

  public static messageKey_OBJECT = "OBJECT";
  public static objectSavedSuccessMessage = "<b>OBJECT</b> saved successfully.";
  public static messageType_INFO = "INFO";
  public static messageType_ERROR = "ERROR";
  public static messageType_SUCCESS = "SUCCESS";
  public static messageType_WARNING = "WARNING";

  public static LocalStorage_ManageMenuKey = "collapseManage";

  public static TEXT_NA = 'NA';

  public static gridPageSizeArray = [10, 25, 50, 100];

  public static gridDefaultPageSize = 10;

  public static gridDefaultResizable = true;
  public static gridDefaultSortable = true;
  public static gridDefaultFilter = true;
  public static gridDefaultMinColWidth = 110;
  public static gridDefaultMaxColWidth = 250;
  public static gridDefaultCheckboxColWidth = 35;

  public static gridDefaultLoadingMessage = '<span class="ag-overlay-loading-center"><i class="fa fa-spinner fa-spin ms-2"></i> Fetching data...</span>';

  public static DefaultSkipNumber: number = 0;

  public static MaxPageSize: number = 10000;

  public static states = [
    {
      "name": "Alabama",
      "abbreviation": "AL"
    },
    {
      "name": "Alaska",
      "abbreviation": "AK"
    },
    {
      "name": "American Samoa",
      "abbreviation": "AS"
    },
    {
      "name": "Arizona",
      "abbreviation": "AZ"
    },
    {
      "name": "Arkansas",
      "abbreviation": "AR"
    },
    {
      "name": "California",
      "abbreviation": "CA"
    },
    {
      "name": "Colorado",
      "abbreviation": "CO"
    },
    {
      "name": "Connecticut",
      "abbreviation": "CT"
    },
    {
      "name": "Delaware",
      "abbreviation": "DE"
    },
    {
      "name": "District Of Columbia",
      "abbreviation": "DC"
    },
    {
      "name": "Federated States Of Micronesia",
      "abbreviation": "FM"
    },
    {
      "name": "Florida",
      "abbreviation": "FL"
    },
    {
      "name": "Georgia",
      "abbreviation": "GA"
    },
    {
      "name": "Guam",
      "abbreviation": "GU"
    },
    {
      "name": "Hawaii",
      "abbreviation": "HI"
    },
    {
      "name": "Idaho",
      "abbreviation": "ID"
    },
    {
      "name": "Illinois",
      "abbreviation": "IL"
    },
    {
      "name": "Indiana",
      "abbreviation": "IN"
    },
    {
      "name": "Iowa",
      "abbreviation": "IA"
    },
    {
      "name": "Kansas",
      "abbreviation": "KS"
    },
    {
      "name": "Kentucky",
      "abbreviation": "KY"
    },
    {
      "name": "Louisiana",
      "abbreviation": "LA"
    },
    {
      "name": "Maine",
      "abbreviation": "ME"
    },
    {
      "name": "Marshall Islands",
      "abbreviation": "MH"
    },
    {
      "name": "Maryland",
      "abbreviation": "MD"
    },
    {
      "name": "Massachusetts",
      "abbreviation": "MA"
    },
    {
      "name": "Michigan",
      "abbreviation": "MI"
    },
    {
      "name": "Minnesota",
      "abbreviation": "MN"
    },
    {
      "name": "Mississippi",
      "abbreviation": "MS"
    },
    {
      "name": "Missouri",
      "abbreviation": "MO"
    },
    {
      "name": "Montana",
      "abbreviation": "MT"
    },
    {
      "name": "Nebraska",
      "abbreviation": "NE"
    },
    {
      "name": "Nevada",
      "abbreviation": "NV"
    },
    {
      "name": "New Hampshire",
      "abbreviation": "NH"
    },
    {
      "name": "New Jersey",
      "abbreviation": "NJ"
    },
    {
      "name": "New Mexico",
      "abbreviation": "NM"
    },
    {
      "name": "New York",
      "abbreviation": "NY"
    },
    {
      "name": "North Carolina",
      "abbreviation": "NC"
    },
    {
      "name": "North Dakota",
      "abbreviation": "ND"
    },
    {
      "name": "Northern Mariana Islands",
      "abbreviation": "MP"
    },
    {
      "name": "Ohio",
      "abbreviation": "OH"
    },
    {
      "name": "Oklahoma",
      "abbreviation": "OK"
    },
    {
      "name": "Oregon",
      "abbreviation": "OR"
    },
    {
      "name": "Palau",
      "abbreviation": "PW"
    },
    {
      "name": "Pennsylvania",
      "abbreviation": "PA"
    },
    {
      "name": "Puerto Rico",
      "abbreviation": "PR"
    },
    {
      "name": "Rhode Island",
      "abbreviation": "RI"
    },
    {
      "name": "South Carolina",
      "abbreviation": "SC"
    },
    {
      "name": "South Dakota",
      "abbreviation": "SD"
    },
    {
      "name": "Tennessee",
      "abbreviation": "TN"
    },
    {
      "name": "Texas",
      "abbreviation": "TX"
    },
    {
      "name": "Utah",
      "abbreviation": "UT"
    },
    {
      "name": "Vermont",
      "abbreviation": "VT"
    },
    {
      "name": "Virgin Islands",
      "abbreviation": "VI"
    },
    {
      "name": "Virginia",
      "abbreviation": "VA"
    },
    {
      "name": "Washington",
      "abbreviation": "WA"
    },
    {
      "name": "West Virginia",
      "abbreviation": "WV"
    },
    {
      "name": "Wisconsin",
      "abbreviation": "WI"
    },
    {
      "name": "Wyoming",
      "abbreviation": "WY"
    }
  ]

  public static defaultCountry: string = 'USA';

  public static formUniqueIdValidationExp = "[0-9]{1,5}";
  public static formZipValidationExp = "[0-9]{5}";
  public static formTagsValidationExp = "[0-9]{1,2}";
  public static formPhoneValidationExp = "^((\\+91-?)|0)?[0-9]{10}$";
  public static  formEmailValidationExp = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";

  public static ComponentKeySearchUsers: string = "search-users";
  public static ComponentKeyUsersRealms: string = "users-realms";
  public static ComponentKeySearchUsersEditRealm: string = "edit realm-users";
  public static ComponentKeySearchClientsEditRealm: string = "edit realm-clients";

  public static ComponentKeyAddUsersToRealm: string = "add-users-to-realm";
  public static ComponentKeyAddClientsToRealm: string = "add-clients-to-realm";
  public static ComponentKeyAddRolesToRealm: string = "add-roles-to-realm";
  public static ComponentKeyAddPermissionsToRealm: string = "add-permissions-to-realm";
  public static ComponentKeyEditRolesInRealmAddPermissions: string = "edit-roles-in-realm-add-permissions";

  public static ComponentKeySearchClients: string = "search-clients"
  public static ComponentKeyClientsRealms: string = "client-realms";

  public static ComponentKeySearchRoles: string = "search-roles"
  public static ComponentKeySearchPermissions: string = "search-permissions"
  public static ComponentKeySearchRolesEditRealm: string = "edit realm-roles"
  public static ComponentKeySearchPermissionsEditRealm: string = "edit realm-permissions"

  public static ComponentKeyAddClient: string = "add-client";


  public static ComponentKeyAddRealm: string = "add-realm";
  public static ComponentKeyViewRealmPermissions: string = "realm-permissions"

  public static ActionKeyNew: string = "new";
  public static ActionKeyEdit: string = "edit";
  public static ActionKeyDelete: string = "delete";
  public static ActionKeyAdded: string = "added";
  public static ActionKeyUpdated: string = "updated";
  public static ActionKeyDeleted: string = "deleted";
  public static PROP_DoNotShowSavePopUp: string = "doNotShowSavePopUp";

  public static COMPONENTKEY: string = "COMPONENTKEY";
  public static PRIMARYKEY: string = "PRIMARYKEY";
  public static PRIMARYVALUE: string = "PRIMARYVALUE";

  public static TYPE__STRING: string = "string";

  public static PK_AirportId: string = "airportId";
  public static PK_BaseId: string = "baseId";
  public static PK_ClientProductId: string = "clientProductId";
  public static PK_GatewayProductId: string = "gatewayProductId";
  public static PK_GatewayClientProductId: string = "gatewayClientProductId";
  public static PK_GatewayId: string = "gatewayId";
  public static PK_InvoiceMetaDataTypeId: string = "invoiceMetaDataTypeId";
  public static PK_InvoiceComponentMetaDataTypeId: string = "invoiceComponentMetaDataTypeId";
  public static PK_GatewayClientTaxId: string = "gatewayClientTaxId";
  public static PK_GatewayTaxId: string = "gatewayTaxId";
  public static PK_ClientTaxId: string = "clientTaxId";
  public static PK_PaymentTypeId: string = "paymentTypeId";
  public static PK_TenderTypeId: string = "tenderTypeId";
  public static PK_Zero: number = 0;
  public static PK_OracleSignetBaseId: string = "oracleSignetBaseId";
  public static PK_MerchantId: string = "merchantId";
  public static PK_ModuleId: string = "moduleId";
  public static PK_ValidationRuleId: string = "validationRuleId";
  public static PK_ValidationRuleConfigurationId: string = "validationRuleConfigurationId";
  public static PK_ConfigurationItemId: string = "configurationItemId";
  public static PK_ActionId: string = "actionId";
  public static PK_InvoiceTypeId: string = "invoiceTypeId";
  public static PK_ModuleValidationRuleId: string = "moduleValidationRuleId";

  public static ID_BaseId: string = "baseId";
  public static ID_Name: string = "name";
  public static ID_ProductCode: string = "productCode";
  public static ID_ClientProductId: string = "clientProductId";
  public static ID_GatewayProductId: string = "gatewayProductId";
  public static ID_PaymentTypeId: string = "paymentTypeId";
  public static ID_GatewayId: string = "gatewayId";
  public static ID_Tags: string = "tags";
  public static ID_GatewayKey: string = "gatewayKey";
  public static ID_InvoiceMetaDataTypeId: string = "invoiceMetaDataTypeId";
  public static ID_InvoiceComponentMetaDataTypeId: string = "invoiceComponentMetaDataTypeId";
  public static ID_ClientTaxId: string = "clientTaxId";
  public static ID_GatewayTaxId: string = "gatewayTaxId";
  public static ID_TaxCode: string = "taxCode";
  public static ID_SettlementGroupId: string = "settlementGroupId";
  public static ID_TenderTypeId: string = "tenderTypeId";
  public static ID_OracleGLCode: string = "oracleGLCode";
  public static ID_SignetBaseId: string = "signetBaseId";
  public static ID_MerchantNumber: string = "merchantNumber";
  public static ID_MerchantId: string = "merchantId";
  public static ID_AirportId: string = "airportId";
  public static ID_ConfigurationItemId: string = "configurationItemId";
  public static ID_ModuleId: string = "moduleId";
  public static ID_InvoiceTypeId: string = "invoiceTypeId";
  public static ID_ValidationRuleId: string = "validationRuleId";
  public static ID_ActionId: string = "actionId";

  public static ID_User: string = "user";
  public static ID_Client: string = "client";
  public static ID_Role: string = "role";
  public static ID_Permission: string = "permission";


  public static ERROR_LoginFailed: string = "Error: Login attempt failed. Please check your credentials.";
  public static ERROR_KEY: string = "error";

}
