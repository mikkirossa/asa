import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map, mergeMap } from 'rxjs/operators';
import { AuthService } from './service/auth.service';
import { CacheService } from './service/cache.service';
import { UserService } from './service/user.service';
import { Constants } from './utils/constants';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'asa-ui';
  minibar: boolean = false;
  showLayout: boolean = false;
  breadCrumb!: string;
  constructor(private router: Router,
    private authService: AuthService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private cacheService: CacheService) {
    //this.minibar = this.cacheService.getUserSettings()?.showSideBar == 'false' ? true : false;
  }

  showSideBar() {
    let minimizeSidebar = this.userService.minimizeSidebar();
    //if (minimizeSidebar == null || minimizeSidebar == undefined)
    //  setTimeout(() => { this.showSideBar(); }, 100);
    //else {
      this.cacheService.cacheData();
      this.minibar = minimizeSidebar;
      console.log('this.minibar', this.minibar);
    //}
  }

  //getUserSettings() {
  //  return (this.userService.getUserSettings())?.filter((t: any) => t.key == Constants.User_SettingsKey)[0]?.settings;
  //}

  handleSideBar(minibar: any) {
    this.minibar = (minibar == "true");
  }

  ngOnInit() {
    console.log(this.router);
    if (!this.router.url.toLowerCase().includes('login')) {
      this.authService.validateToken(true);
      this.showSideBar();
    }

    this.router.events.pipe(
      filter(events => events instanceof NavigationEnd),
      map(evt => this.activatedRoute),
      map(route => {
        while (route.firstChild) {
          route = route.firstChild;
        }
        return route;
      }))
      .pipe(
        filter(route => route.outlet === 'primary'),
        mergeMap(route => route.data)
      )
      .subscribe((x: any) => {
        x.layout === true ? this.showLayout = true : this.showLayout = false;
        this.breadCrumb = x.breadCrumb;
      })
  }

}
